var integer_regex =   /^[0-9.]+$/;
var aplha_regex =   /^[a-zA-Z]+$/;
var regex_list = {integer:integer_regex,alpha:aplha_regex};
$(function(){
    if($("input[type='idate']").length){
        $.each($("input[type='idate']"),function(k,v){
            var parent = $(v).parent();
            parent.append("<div class='input-group'>");
            parent.find(".input-group").append($(v));
            var ele = $('<div class=\"input-group-btn\">');
            ele.append(parent.find("a"));
            ele.find("a").html('<button class="btn btn-default" type="button"><i class="fa fa-calendar" aria-hidden="true"></i></div>');
            parent.find(".input-group").append(ele);
            // parent.find(".input-group-btn").append('<button class="btn btn-default" type="button">Go!</button>');
            // parent.find(".input-group-btn").append();

        });
    }
    tinyMCE.init({
        theme : "modern",
        mode: "specific_textareas",
        toolbar: "preview",
        resize: 'both',
        content_css : 'css/bootstrap.min.css',
        editor_selector : "ihtml",
        plugins : [ "code visualchars advlist table spellchecker textcolor searchreplace preview" ],
        height:"auto",
        width:"auto",
        formats:{
            underline: { inline: 'u', exact: true }
        }
    
    });
    $('.scrollmenu a[data-qual]').click(function(){
        var qual = $(this).attr("data-qual");
        $('#qual_field').val(qual);
        $('#frm1').submit();
;    });
});
function customAlert(msg,cb){
    $('#_alert-modal .modal-title').text('Alert!');
    $('#_alert-modal .modal-body p').html(msg);
    $('#_alert-alert').show();
    $('#_alert-confirm').hide();
    $('#_alert-modal').modal('show');
    $('#_alert-modal #_alert-modal-ok,#_alert-modal #_alert-modal-cancel').off().on('click',function(){
        $('#_alert-modal').modal('hide');
        cb(true);
    });
}
function successAlert(msg,cb){
    $('#_alert-modal .modal-title').text('Success!');
    $('#_alert-modal .modal-body p').html(msg);
    $('#_alert-alert').show();
    $('#_alert-confirm').hide();
    $('#_alert-modal').modal('show');
    $('#_alert-modal #_alert-modal-ok,#_alert-modal #_alert-modal-cancel').off().on('click',function(){
        $('#_alert-modal').modal('hide');
        cb(true);
    });
}

function customConfirm(msg,cb){
    $('#alert-modal .modal-title').text('Confirmation');
    $('#alert-modal .modal-body p').text(msg);
    $('#alert-alert').hide();
    $('#alert-confirm').show();
    $('#alert-modal').modal('show');

    $('#alert-modal #alert-confirm-true').off().on('click',function(){
        $('#alert-modal').modal('hide');
        cb(true);
    });
    $('#alert-modal #alert-confirm-false').off().on('click',function(){
        $('#alert-modal').modal('hide');
        cb(false);
    });
}

function customeDateTimepicker(elem,format){
    $('#'+elem).daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        // "timePicker": true,
        // "timePicker24Hour": true,
        "autoApply": true,
        startDate:moment(),
        useCurrent: true,
        locale: {
            format: 'DD-MMM-YYYY'
      }
    });
    $('#'+elem).focus();
}
if($('.redirect').length){
	var tbl =  $('.redirect').attr('data-table');
	successAlert("Inventory data uploaded successfully",function(){
		window.location.href = 'home.php?page='+tbl;
	})
	// console.log('table:'+tbl);
}


function inrCurrency(x){
    x=x.toString();
    var afterPoint = '';
    if(x.indexOf('.') > 0)
    afterPoint = x.substring(x.indexOf('.'),x.length);
    x = Math.floor(x);
    x=x.toString();
    var lastThree = x.substring(x.length-3);
    var otherNumbers = x.substring(0,x.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
    return res;
}

function round(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
  }

  function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function ajaxCall(url,data,that,cb){
    $.ajax({
        type : 'post',
        data : data,
        url : url,
        beforeSend: function(){
            $(that).prop('disabled',true);
        },
        complete: function(){
            $(that).prop('disabled',false);

        },
        success : function(res){
            if(res){
                res = $.parseJSON(res);
                cb(res);
            }else{
                cb();
            }
        }
    });
}

function ValidationData(data){
    $.each(required_fields,function(k,v){
        var elem = $("input[name="+k+"0]");
        if(elem.length){
            elem = $("select[name="+k+"0]");
        }
        var flag = false;
        if(elem.length){
            if(!elem.val()){
                flag = true;


            }
        }
    });
}