$(function(){
    $('#addnewoption-modal').on('show.bs.modal',function(event){
        var btn = $(event.relatedTarget);
        PopulateAddNewOption(btn);
    });
    $('#addnewoption-modal').on('hide.bs.modal',function(){
        $(".addnewoption-body").html('');
    });
})
function addNewOption(data){
   $.each(data,function(k,v){
        var field = v.field;
        field = field+'0';
        var ele = $('#'+field);
        var attr = "data-field='"+v.field+"'";
        attr += " data-cur_tbl='"+v.cur_tbl+"'"
        attr += " data-destination='"+v.destination+"'"
        attr += " data-showfields='"+v.showfields+"'"
        attr += " data-hiddenfields='"+JSON.stringify(v.hiddenfields)+"' "
        ele.parent().append("<a href='#' class='add-new' data-target='#addnewoption-modal' data-toggle='modal' "+attr+">Add New</a>");
   });
    
}

function saveAddNewOption(){
    var url = 'addnewoption.php';
    var all_data = $(".addnewoption-body input");
    var data = new Array();
    $.each(all_data,function(k,v){
        var name = $(v).attr('name');
        var val = $(v).val();
        data.push({name:name,value:val});
    });
    that = $('#addnewoption-btn')[0];
    ajaxCall(url,data,that,function(res){
        if(res.success){
            var field = $('.addnewoption-body input[name=field]').val();
            $('#'+field+'0').html(res.res);
        }
        $('#addnewoption-modal').modal('hide');
    });
}

function PopulateAddNewOption(ele){
    var field = ele.data('field');
    var cur_tbl = ele.data('cur_tbl');
    var destination = ele.data('destination');
    var showfields = ele.data('showfields');
    var hiddenfields = ele.data('hiddenfields');
    if(!hiddenfields){
        hiddenfields = {};
    } 
    var h_fileds = "<input type='hidden' name='field' value='"+field+"'>";
    h_fileds += "<input type='hidden' name='cur_tbl' value='"+cur_tbl+"'>";
    h_fileds += "<input type='hidden' name='destination' value='"+destination+"'>";
    h_fileds += "<input type='hidden' name='showfields' value='"+showfields+"'>";
    $('.addnewoption-body').html('');
    $('.addnewoption-body').append(h_fileds);
    var fields = showfields.split(',');
    
    $.each(fields, function(k,v){
        var hide = '';
        var val = '';
        if(hiddenfields[v] !== undefined){
            hide ='hide';
            val = hiddenfields[v];
        } 
        var alias = v.replace('_',' ');
        var formfield = '<div class="form-group '+hide+'">';
        formfield += '<label for="search-name">'+alias+'</label>';
        formfield += '<input type="text" class="form-control" name="'+v+'" placeholder="'+alias+'" value="'+val+'">';
        formfield += '</div>';
        $('.addnewoption-body').append(formfield);
    });



}