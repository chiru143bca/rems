(function ( $ ) {

    $.fn.validate = function(option,prefix){
        var err = "";
        if(option){
            var that = this;
            $.each(option.rules,function(k,v){
                // check if ihtml
                if(tinyMCE.get(k+'0')){
                    var tm_val = tinyMCE.get(k+'0').getContent();
                    $(that).find("textarea[name="+k+"0]").val(tm_val.trim());
                }
                var elem = $(that).find("input[name="+k+"0]");
                if(!elem.length){
                    elem = $(that).find("select[name="+k+"0]");
                }
                if(!elem.length){
                    elem = $(that).find("textarea[name="+k+"0]");
                }
                // loop each field rule
                var res = false;
                $.each(v,function(key,msg){
                    switch (key){
                        case 'reqired':
                           res = requiredField(elem);
                            break;
                        case 'positvie_number':
                           res = positveNumber(elem);
                            break;
                        case 'greaterthan_today':
                           res = greaterthanToday(elem);
                            break;
                        default:
                            res = requiredField(elem);
                            break;
                    }
                    if(res) err += msg+'<br>';
                });

            });
        }
        if(err) return err;
        else return false;
        
    };  
    requiredField = function(field){
        if(field.length && !field.val().trim()){
            return true;
        }else return false;
    };
    positveNumber = function(field){
        if(field.length && field.val()){
            if(isNaN(field.val())){
                return true;
            }else if(field.val() < 0){
                return true;
            }
            else{
                return false;
            }
        }else return false;
    };
    greaterthanToday = function(field){
        if(field.length && field.val()){

        }
    };

})(jQuery)