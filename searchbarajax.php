<?php
session_start();
require_once('lib/DbModel.php');
require_once('lib/Utils.php');
$db_model = new DbModel();
$utils = new Utils();
$selectbox ="<select name=\"data\" style=\"height: 26px;\">";
$list = "<option value=\"\"> -- clear all --<option>";
if($_POST){
    $options = $utils->getSelectBoxData($_POST['type'],$_POST['fieldid']);
    if($options && !empty($options)){
        foreach($options as $option){
            $list .= "<option value=\"".$option['value']."\">".$option['alias']."</option>";
        }
    }
}
$selectbox .= $list."</select>";

echo $selectbox;