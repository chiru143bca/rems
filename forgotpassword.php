<html>
<head>
	<title>Input Zero Technologies Pvt Ltd</title>
	<link rel="stylesheet" type="text/css" href="css/login.css" />
	<link href="css/bootstrap.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link href="css/style.css" rel="stylesheet"/>
</head>

<body >
<center>
    <div class="container-fluid">
    	<div id="top_left">
			
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<br/><br/><br/><br/>
                </div>
	</div>
	<div class="row" >
		<div class="col-md-4">
		</div>
		<div id="121" class="col-md-4">
			<form role="form" method="POST" action="" class="modal-content" id="forgot-form">
				<div class="textblock">	
				<img id="img" src="images/logo.png" alt="Input Zero" />
					<div class="ubheader">
					<h3><center>  InputZero Technologies Pvt. Ltd </center><div style="height:5px;"></div><center style="font-size:15px;">ATS</center></h3>
					</div>
					<div class="maincntn">
						<hr />
						<h5><strong>RESET PASSWORD</strong></h3>
						<div id="textblock">
							<input type="text" name="mobile" placeholder="Enter Mobile No."><br/><br/>
							<button type="submit" class="btn btn-primary fa fa-envelope" id="forgot-form"> Send OTP</button>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="col-md-4">
		</div>
	</div>
	<div id="footer">
		<strong>
			&copy; InputZero Technologies
		</strong>
	</div>
</center>
<?php include('alert-modal.html'); ?>
<script language="javascript" type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
<script language="javascript" type="text/javascript" src="js/tinymce/custom.js"></script>
<script language="javascript" type="text/javascript" src="js/scripts.js"></script>
<script>
	function cb(){};
$(function(){
	$('#forgot-form').submit(function(e){
		e.preventDefault();
		var mobile = $("input[name=mobile]").val();
		if(!mobile){
			customAlert('Mobile is required.',cb);
			return false;
		}
		$.ajax({
			type:'post',
			data: {mobile:mobile,type:'forgot'},
			url: 'reset-password-ajax.php',
			beforeSend: function(){
				$('#optButton').button('loading');
			},
			complete: function(){
				$('#optButton').button('reset');
			},
			success : function(res){
				if(res){
					var res = $.parseJSON(res);
					if(res.success){
						successAlert(res.success,function(res){
							if(res) window.location.href="reset-password.php";
						});
					}else if(res.error){
						customAlert(res.error,cb);
						return false;
					}
				}
			}
		});
	});
});
</script>
</body>
</html>
