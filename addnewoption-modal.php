<div class="modal fade" id="addnewoption-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New Option</h4>
      </div>
      <div class="modal-body">
          <div class="col-md-12 addnewoption-body">

              
            </div>
        </div>
            <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addnewoption-btn" onclick="return saveAddNewOption()">Save</button>
      </div>
    </div>
  </div>
</div>