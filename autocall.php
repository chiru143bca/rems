<?php
require_once('lib/DbModel.php');
require_once('lib/Utils.php');
require_once('mail_function.php');
$db_model = new DbModel();
$utils = new Utils();
$utils->write_log("debug","task started".date('Y-m-d H:i:s'));
$notifications = $db_model->allArray("select * from notification_log where send_status = 'initiated'");
$utils->write_log("debug",json_encode($notifications));
// print_r($notifications);
$seconds = 20;
foreach($notifications as $notification){
    $time = time();
    $no_id = $notification['id'];
    $attachments = array();
    if($notification['attachment']) $attachments = explode(',',$notification['attachment']);
    $cc = array();
    if(isset($notification['cc']) && $notification['cc']) $cc = explode(',',$notification['cc']);
    // $utils->write_log("debug","1111:".json_encode($attachments));
    $status = send_mail(array($notification['recipient_email']), array($notification['recipient']), array($notification['notification_subject']), array($notification['template']), $seconds,$attachments,$cc);
    $db_model->executeQuery("update notification_log set send_status = '{$status}', modified_at = '{$time}',mails_sent = '{$time}' where id = {$no_id}");
}
$logtime = date('H:i');
if($logtime == '00:00'){
    copy('logs/logs.log', 'logs/log_'.date('y_m_d',strtotime("-1 day")).'.log');
    file_put_contents('logs/logs.log','');
}