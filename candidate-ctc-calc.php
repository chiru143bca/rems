<?php
session_start();
require_once('lib/CoreView.php');
require_once('lib/Utils.php');
$coreV = new CoreView();
$utils = new Utils();
// $sql1=$coreV->dbsql();
$user_id = $_SESSION['SESS_id'];
$band = $_POST['band'];
$template = $_POST['template'];
$ctc = $_POST['ctc'];
$attribute_sql = "select * from attributes where band = '{$band}' and template_name = '{$template}' order by sorting asc";
$attributes = $utils->allArray($attribute_sql);
$ctc_array = array('attribute'=>'ctc','formula'=>$ctc,'alias'=>'CTC');
array_unshift($attributes,$ctc_array);
$components = $utils->array_columns($attributes,'formula','attribute');
foreach($components as $key=>$value){
    $components[$key] = round($utils->complieDataVar($components,$value));
}
echo json_encode($components);
exit;
