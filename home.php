<?php
ini_set("display_errors", "1");
error_reporting(E_ALL);
date_default_timezone_set("Asia/Kolkata");
require_once("auth.php");
require_once('lib/csrf.php');	
include('excel-export.php');
// require_once("validation.php");
require_once('lib/DbModel.php');
require_once('lib/SearchHistory.php');
require_once('lib/widgets/WidgetLib.php');
$csrf = new csrf();
$db_model = new DbModel();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>InputZero Technologies Pvt. Ltd</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Tell the browser to be responsive to screen width -->
  <!-- <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> -->
  <link rel="stylesheet" type="text/css" href="css/templateblue.css" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
	<!-- <link href="css/bootstrap.min.css" rel="stylesheet"/> -->
	<!-- <link href="css/style.css" rel="stylesheet"/> -->
	<link href="css/sbar.css?d=<?php echo time(); ?>" rel="stylesheet"/>
	<link href="css/toggleswitch.css" rel="stylesheet"/>
	<link href="plugin/jquery-ui-1.11.4/jquery-ui.css" rel="stylesheet"/>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css"> -->
  <!-- jvectormap -->
  <!-- <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <!-- <link rel="stylesheet"  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png"/>
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png"/>
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png"/>
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png"/>
  <link rel="shortcut icon" href="img/favicon.png"/>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
  <script type="text/javascript" src="plugin/jquery-ui-1.11.4/jquery-ui.js"></script>
	<script type="text/javascript" src="app/orders/orders_js.js"></script> 
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <style>
  @print{
    @page :footer {color: #fff }
    @page :header {color: #fff}
}
@page { size: auto;  margin: 12mm; }
@media print {
  body {
    margin: 0;
    color: #000;
    background-color: #fff;
  }
  title{
    color:red;
  }
}
.form-horizontal .form-group {
    margin-right: 0px;
    margin-left: 0px;
}
  </style>
</head>
<body class="hold-transition sidebar-mini">
<!-- <body class="hold-transition sidebar-mini sidebar-collapse"> -->
<input type="hidden" name="_tbl" value="<?php echo $_GET['page']; ?>">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <!-- <a href="home.php?page=text.php" class="logo"> -->
    <a href="home.php?page=text.php" class="logo hidden-sm hidden-xs">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><img src="images/logo.png"/></b></span>
      <!-- logo for regular state and mobile devices -->
      <!-- <span class="logo-lg"><img src="images/UB_icon.png" alt=" "></span> -->
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <!-- <a href="#" class="sidebar-toggle hide" data-toggle="push-menu" role="button"> -->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        <?php include("lib/nav.php"); ?>
        <li><a href="home.php?page=reports">Reports</a></li>
        <!-- <li ><a href="#"><span class="align-middle">Hi <?php echo $_SESSION['SESS_ename'];?>&nbsp;!</span></a></li>
        <li><a href="logout.php">Logout</a></li> -->
        <li role="presentation" class="dropdown"> 
          <a href="#" class="dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false">Hi <?php echo $_SESSION['SESS_ename'];?> <span class="caret"></span></a> 
          <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents"> 
            <li><a href="logout.php"  aria-controls="dropdown1">Logout</a></li> 
            </ul> 
        </li>
        <li><div class="col-md-1"></div></li>
        </ul>
         
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
  <!-- <img src="images/UB_icon.png" alt=" "> -->
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
         
         
      </div>
       
      <?php
		require_once("auth.php");
		?>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <!-- <ul class="sidebar-menu hide" data-widget="tree"> -->
      <ul class="sidebar-menu" data-widget="tree">
        <?php include("lib/menu.php"); ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <div class="col-md-12">
						<?php
							echo "<form role=\"form\" class=\"form-horizontal\" enctype=\"multipart/form-data\" name=\"form1\" id=\"frm1\" action=\"\" method=\"POST\">";
							//echo "<input type=\"text\" id=\"uid\" value=\"\" />";
              echo "<input type=\"hidden\" name=\"main_id\">";
              echo "<input type=\"hidden\" name=\"csrf_token\" value=\"".$csrf->csrf."\">";
							if(isset($_POST['error']) && $_POST['error']):
								// echo $_POST['error'];exit;
                $errors = json_decode($_POST['error']);
                 
								?>
							<div class="alert alert-danger">
              
								<ul>
									<?php  foreach($errors as $err): ?>
									<li><?php echo $err[0]; ?></li>
              
									<?php endforeach; ?>
								</ul>
							</div>
							<?php
              endif;
														// require_once("lib/db_trigger.php");
														include("lib/insert.php");
														
              echo "</form>";
						?>
						</div>
				</div>
  </div>
  <?php include('search_modal'); ?>
  <!-- /.content-wrapper -->
  <div class="clearfix"></div>
  <footer class="main-footer hidden-print">
  <div class="row">
  <div style="position:relative; left: 35%;" class="col-md-4 col-md-push-3">
  <strong>&copy; InputZero Technologies (Best viewed on 1600*900)</strong>
  </div>
  </div>
    
  </footer>

  
   
</div>
<?php include('alert-modal.html'); ?>
<?php include('customer-related-modal.php'); ?>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<!-- <script src="bower_components/jquery/dist/jquery.min.js"></script> -->
<!-- Bootstrap 3.3.7 -->
<!-- <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script> -->
<!-- FastClick
<script src="bower_components/fastclick/lib/fastclick.js"></script> -->
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- Sparkline
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script> -->
<!-- jvectormap 
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> -->
<!-- SlimScroll -->
<!-- <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script> -->
<!-- ChartJS
<script src="bower_components/chart.js/Chart.js"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes)
<script src="dist/js/pages/dashboard2.js"></script> -->
<!-- AdminLTE for demo purposes
<script src="dist/js/demo.js"></script> -->
<script language="javascript" type="text/javascript" src="datetimepick/datetimepicker.js"></script>

<script language="javascript" type="text/javascript" >
if(document.forms["frm1"].chb){
	document.forms["frm1"].chb.onchange= function(){
	var j = document.getElementById("<?php echo $tbl; ?>").rows.length-1; 
	for(i=0;i<j;i++) { 
		var z=document.forms["frm1"].elements["chb"+i].checked=eval(document.forms["frm1"].chb.checked);  
	}
	} 
}
</script>
<script>
function openPage($var,$tb)
{
var x=$var;
var t=$tb;
document.forms["frm1"].action="home.php?page="+t+"&num="+x;
document.forms["frm1"].submit();
}
function openRec($var,$tb)
{
var x=$var;
var t=$tb;
//document.forms["frm1"].elements['uid'].value=x;
var num = getUrlVars()['num'];
document.forms["frm1"].elements['sel'].value=" id="+x;
document.forms["frm1"].elements['main_id'].value=x;
if(num) document.forms["frm1"].action="home.php?page="+t+"&num="+num;
else document.forms["frm1"].action="home.php?page="+t;

$('#frm1').append("<input type='hidden' value='click' name='act'/>");
document.forms["frm1"].submit();
}
$('#menu-toggle').click(function(){
	$('#sidebar-toggle').toggleClass('hide-toggle');
	$('#wrap-content').toggleClass('col-md-8');
	$.ajax({
		url:'simple-request.php',
		data:{type:'toggle'},
		method:'get',
		success:function(){

		}
	});
});
$(function(){
  $(document).keypress(
    function(event){
      if (event.which == '13') {
        event.preventDefault();
      }
  });
  $("#realtedForm ").on('click','td',function(){
     var data_id = $(this).find("span[data-id]");
     if(data_id.length){
       var x = data_id.attr('data-id');
       var t = data_id.attr('data-tbl');
       var sel = $("#frm1 input[name=sel]").val();
       var main_id = $("#frm1 input[name=main_id]").val();
       document.forms["frm1"].elements['sel'].value=" id="+x;
      document.forms["frm1"].elements['main_id'].value=x;
      document.forms["frm1"].action="home.php?page="+t;
      $('#frm1').attr('target','_blank');
      $('#frm1').append("<input type='hidden' value='click' name='act'/>");
      document.forms["frm1"].submit();
      $('#frm1').attr('target','');
      $('#frm1').attr('action','');
      $("#frm1 input[name=sel]").val(sel);
      $("#frm1 input[name=main_id]").val(main_id);
     }
  });
  var qual_val = $("#qual_field").val();
 if(qual_val){

   qual_val = qual_val.trim();
   $('.scrollmenu a[data-qual="'+qual_val+'"]').addClass('active');
  }
  $("a.tiles").click(function(e){
        e.preventDefault();
        var query = $(this).attr('data-ref');
        // console.log(query);return false;
        if(query) query = escapeHTML(query);
        // console.log(query);
        // query = "status = \'6\'";
        $("#frm2").append("<input type='hidden' name='qual_field'>");
        $("#frm2 input[name=qual_field]").val(query);
        $("#frm2").submit();
        // console.log(query);
        // window.location.href = 'home.php?page=invoice&qual='+query;
    });
});

function escapeHTML(text)
    {
       var chr = { '"': '"', '&': '&', '<': '<', '>': '>' };
       function abc(a)
       {
          return chr[a];
       }
       return text.replace(/[\"&<>]/g, abc);
    }
</script>
<form id="frm2" method="post" action=""></form>
<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="css/daterangepicker.css" />
<script language="javascript" type="text/javascript" src="script/childscript.js"></script>
<script language="javascript" type="text/javascript" src="script/xml.js"></script>
<script language="javascript" type="text/javascript" src="script/basic.js?d=<?php echo time(); ?>"></script>
<script language="javascript" type="text/javascript" src="script/searchbuilder.js?d=<?php echo time(); ?>"></script>
<script language="javascript" type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
<script language="javascript" type="text/javascript" src="js/tinymce/custom.js"></script>
<script language="javascript" type="text/javascript" src="js/scripts.js?d=<?php echo time(); ?>"></script>
<script language="javascript" type="text/javascript" src="lib/widgets/script.js?d=<?php echo time(); ?>"></script>

<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

</body>
</html>
