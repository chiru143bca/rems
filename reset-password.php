<html>
<head>
	<title>Input Zero Technologies Pvt Ltd</title>
	<link rel="stylesheet" type="text/css" href="css/login.css" />
	<link href="css/bootstrap.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link href="css/style.css" rel="stylesheet"/>
</head>

<body >
<center>
    <div class="container-fluid">
    	<div id="top_left">
			
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<br/><br/><br/><br/>
                </div>
	</div>
	<div class="row" >
		<div class="col-md-4">
		</div>
		<div id="121" class="col-md-4">
			<form role="form" method="POST" action="check_login.php" class="modal-content form-horizontal">
				<div class="textblock">	
					<img id="img" src="images/logo.png" alt="Input Zero" />
					<div class="ubheader">
					<h3><center>  InputZero Technologies Pvt. Ltd </center><div style="height:5px;"></div><center style="font-size:15px;">ATS</center></h3>
					</div>
					<div class="maincntn">
                        <hr />
                        <div class="form-group">
                            <label class="pull-left" for="mobile">Mobile</label>
                            <input type="text" class="fomr-control" id="mobile" name="mobile">
                        </div>
                        <div class="form-group">
                            <label class="pull-left" for="otp">OTP</label>
                            <input type="text" class="fomr-control" id="otp" name="otp">
                        </div>
                        <div class="form-group">
                            <label class="pull-left" for="password">New Password</label>
                            <input type="password" class="fomr-control" id="password" name="password">
                        </div>
                        <div class="form-group">
                            <label class="pull-left" for="confirm_password">Confirm Password</label>
                            <input type="password" class="fomr-control" id="confirm_password" name="confirm_password">
                        </div>
						<div class="form-group">
                            <button type="button" class="btn btn-info" id="reset-password">Reset Password</button>
                        </div>
					</div>
				</div>
			</form>
		</div>
		<div class="col-md-4">
		</div>
	</div>
	<div id="footer">
		<strong>
			&copy UB Order Management System
		</strong>
	</div>
</center>
<?php include('alert-modal.html'); ?>
<script language="javascript" type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
<script language="javascript" type="text/javascript" src="js/tinymce/custom.js"></script>
<script language="javascript" type="text/javascript" src="js/scripts.js"></script>
<script>
    function cb(){}
$(function(){
	$('#reset-password').click(function(){
		var mobile = $("input[name=mobile]").val();
		var otp = $("input[name=otp]").val();
		var password = $("input[name=password]").val();
        var confirm_password = $("input[name=confirm_password]").val();
		if(!mobile){
			customAlert('Mobile is required.',cb);
			return false;
        }
        if(!otp){
            customAlert('OTP is required.',cb);
			return false;
        }
        if(!password){
            customAlert('Password is required.',cb);
			return false;
        }
        if(!confirm_password){
            customAlert('Confirm Password is required.',cb);
			return false;
        }
        if(password && confirm_password && password != confirm_password){
            customAlert('Password mismatch.',cb);
			return false;
        }
		$.ajax({
			type:'post',
			data: {mobile:mobile,type:'reset',otp:otp,password:password},
			url: 'reset-password-ajax.php',
			beforeSend: function(){
				$('#reset-password').button('loading');
			},
			complete: function(){
				$('#reset-password').button('reset');
			},
			success : function(res){
				if(res){
					var res = $.parseJSON(res);
					if(res.success){
						successAlert("Reset success",function(res){
                            if(res) window.location.href="index.php";
                        });
					}else{
						customAlert("Faild to reset password",cb);
						return false;
					}
				}
			}
		});
	});
});
</script>
</body>
</html>
