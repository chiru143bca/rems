<?php
require_once('lib/DbModel.php');
require_once('lib/Utils.php');
function createSearchBuilder($tblname,$qual,$toggle=false,$data=false,$search=array())
{   
        $user_id = $_SESSION['SESS_id'];
        $db_model = new DbModel();
        $utils = new Utils();
        $widget = new WidgetLib();
        $widget_btn = $widget->getButton();
        $btn_content="<button class=\"btn btn-danger\" type=\"button\" id=\"content-search\" />Content Search</button>";
        // echo "<br>=====coming========<br>$qual<br>========";
        $collapse ='';
        $conditions = $data?$data->conditions:'';
        if(!$toggle) $collapse ='collapse';
        if(!$toggle) echo "&nbsp;<a class='btn btn-info' data-toggle='collapse' id='filterbtn' href='#search-bar' aria-expanded='false' aria-controls='search-bar'><i class='glyphicon glyphicon-filter'></i>Filter</a>&nbsp;{$widget_btn}<div class='clearfix'></div>";
        echo "<div class=\"searchbuilder {$collapse}\" id=\"search-bar\">";
        $tblid="";
        // $res=mysql_query("select tblid from config where name='$tblname'");
        $sql="select tblid from config where name='$tblname'";
        $result = $db_model->first($sql);
        $tblid = $result->tblid;
        $dbfields=array();$fieldstring="";
        if(!empty($tblid))
        {   
            if(isset($qual) && $qual){
                $qual_orig=$qual;
            }else    
                $qual_orig="1=1";
            if($tblname == 'orders'){
                $s = "select fieldid,name, alias,type from field where tblid = '$tblid'  and dbindex != 'primary' and name in('source_id','customer_id','order_no','order_date','status')";
            }else{
                $s = "select fieldid,name, alias,type from field where tblid = '$tblid'  and dbindex != 'primary' and name not in('created_by','modified_by')";
            }
            $result = $db_model->allArray($s);
                echo "<br>";
                echo "<form method=\"POST\">";
                if(!$toggle)
                {
                    $is_search = false;
                    $content_title = false;
                    $content_field = false;
                    if(!empty($search)){
                        $is_search = true;
                        $content_field = isset($search['field']) ? $search['field']:'';
                        $content_title = isset($search['title']) ? $search['title']:'';
                    } 
                    if(!empty($search)){
                        echo "<input type='text' name='content_search' data-issearch='".$is_search."' data-content-field='".$content_field."' data-table='".$tblname."'  data-title='".$content_title."'>";
                        echo "&nbsp.$btn_content<br>";
                    }
                    echo "<select style='height: 26px;' name='fields'>";
                }    
               else
                  echo "<select style='height: 26px;' name='c_fields'>";
                echo "<option value='' >Select Field</option>";
                if($result)
                {   
                        // while($row = mysql_fetch_array($s)){
                            foreach($result as $row){
                            array_push($dbfields,$row['name']);
                            echo "<option data-type=\"".$row['type']."\" data-id=\"".$row['fieldid']."\" value =\"". $row['name']."\">".$row['alias']."</option>";
                        }
                        echo "</select>&nbsp;";
                        if(!$toggle) echo "<select style='height: 26px;' name='operator'>";
                       else  echo "<select style='height: 26px;' name='c_operator'>";
                        echo "<option value='' >Select Operator</option>
                        <option value='='>   =	</option>
                        <option value='like'>   LIKE	</option>
                        <option value='>'>   >	</option>
                        <option value='>='>  >=	</option>
                        <option value='<'>   <	</option>
                        <option value='<='>  <=	</option>
                        <option value='!='>  !=	</option>
                        <option value='<>'>  <>	</option>
                        <option value='between'>  Between	</option>
                        </select>&nbsp;";
                        $x=isset($_POST['qual_field'])?$_POST['qual_field']:"";
                        // echo $x;
                        $btn_clear="<button class=\"btn btn-danger\" type=\"button\" id=\"clear\" name=\"clear\" value=\"clear\" />Clear</button>";
                        $btn_run ="";
                        if(!$toggle)  $btn_run = "<input class='btn btn-success' type='submit' id=\"run\" name='run' value='RUN'>&nbsp;";
                        if(!$toggle) echo "<span class='data-span'><input type = 'text' name = 'data' placeholder = 'Enter Value'  value = '' autocomplete=\"off\"></span>&nbsp;";
                        else{
                            echo "<input type = 'text' name = 'c_data' placeholder = 'Enter Value'  value = '' autocomplete=\"off\">&nbsp;";

                        } 
                        echo "<select name = 'c_operend' style='height: 26px;display:none' >
                             <option value=''>Select Range</option>
                             <option value='+'>In Next</option>
                             <option value='-'>In Past</option>
                            </select>&nbsp;&nbsp;";
                        echo "<select name = 'c_data' style='height: 26px;display:none' >
                             <option value=''>----Select time ----</option>
                             <option value='today'>today</option>
                            <option value='1 days'>1 day</option>
                            <option value='7 days'>7 days</option>
                            <option value='15 days'>15 days</option>
                            <option value='1 months'>one month</option>
                            </select>";
                            echo "<span class='relative-div' style='display:none'><input type='checkbox' name='dateType' value='relative'>Relative&nbsp;</span>";

                        if(!$toggle) echo "<input class='btn btn-default' type='button' id = 'and' name='and' value='AND'>&nbsp;";
                       else echo "<input class='btn btn-default' type='button' id = 'c_and' name='c_and' value='AND'>&nbsp;";
                       if(!$toggle) echo "<input class='btn btn-default' type='button' id = 'or' name='or' value='OR'>&nbsp;";
                        else echo "<input class='btn btn-default' type='button' id = 'c_or' name='c_or' value='OR'>&nbsp;";
                        if($toggle)  echo "<input class='btn btn-success' type='button' id=\"add_qual\" name='add_qual' value='Add'><br>";
                        else  echo "<input class='btn btn-success' type='button' id=\"add_qual_main\" name='add_qual' value='Add'><br>";
                        echo $btn_clear;
                        
                        echo "<p style='color:red' id='demo'></p>";
                        if(!$toggle)  echo "<input type = 'text' id = 'qual_field' name = 'qual_field' autocomplete=\"off\" value=\" $x \"  style='width:50%'>&nbsp";
                        if($toggle)  echo "<input type = 'text' id = 'conditions' name = 'conditions' autocomplete=\"off\" value=\" $conditions \"  style='width:50%'>&nbsp";
                        echo "<input type = 'hidden' id = 'qual_hidden' placeholder='hidden' name = 'qual_hidden' value=\" $x \"  style='width:50%'>&nbsp";
                        echo $btn_run."<button class='btn btn-default' type='button' id = 'plus' name='plus'  data-toggle='modal' data-target='#search-log'>+</button>&nbsp";
                        if(!$toggle){
                            $sql = $db_model->All("select * from search_criteria where page='{$tblname}' and (created_by = {$user_id} or is_common = 1)");
                            echo "&nbsp;<select name='searched_query' style='height:28px'><option value=''>---History Search ---</option>";
                                foreach($sql as $_re){
                                echo "<option value='".htmlspecialchars($_re->criteria,ENT_QUOTES)."'>".$_re->name."</option>";
                            }
                            echo "</select>";
                        }
                        // echo "</form>";
                }
                else
                    echo "" ;  
                $field_show=array($fieldstring);
                if(isset($_POST['qual_field']) && $_POST['qual_field']) 
                    $qual=$_POST['qual_field'];    
                // echo "<br>=====returning========<br>$qual<br>========";
                echo $widget->widgetContent();
                echo "</div>";
            $qual = $utils->extractDateFromQual($qual);
            return $qual;
        }
}
?>