<?php
   require_once('lib/DbModel.php');
   $db_model = new DbModel();
   $order_id=$_POST['orderid'];
   $remarks_editability= isset($_POST['editability'])? $_POST['editability'] : 'no' ;
   $a="";$tr="";
   if(isset($_POST) && $_POST['tbl']){
       if($order_id){
   
           $sql1="select comments.user_id,comments.description,comments.created_at,comments.attachments,comments.file_name,users.empname,users.id from users,comments where ( orderno={$order_id} and users.id = comments.user_id) order by comments.created_at desc";
           $res1=$db_model->all($sql1);
           if($res1){
               foreach($res1 as $data){
                   $data->created_at = date('d-m-Y H:i',$data->created_at);
                   $file ="";
                   if($data->attachments){
                       $file = "<a href=\"".$data->attachments."\" download>".$data->file_name."</a>";
                    }
                    $tr .= "<tr><td>".$data->empname."</td><td>".$data->created_at."</td><td>".$data->description."</td><td>".$file."</td></tr>";
                }
            }
        }
         
        $a .= "<div class='col-md-12'>";
        if($remarks_editability=="yes")
            $a .= "<h5><b>Remarks</b> <a href=\"#\" data-toggle='modal' data-target='#remark-modal' class=\"text-success\"><i class=\"fa fa-plus-circle\"></i></a></h5>";
        else if($remarks_editability=="no")
            $a .= "<h5><b>Remarks</b></h5>";
        $a .= "</div>";
        $a.="<div class=\" col-md-12\" style=\"width:97%\">";
        $a .= "<table class='table table-stripe remark-table'>";
        $a .="<thead><tr><th>User</th><th>Date and Time</th><th>Description</th><th>Attachments</th></tr></thead> ";
        $a=$a.$tr." </table>";
        $a .= "</div>";
        echo $a;
    }
?>