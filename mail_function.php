<?php

require_once ('mail/class.phpmailer.php');
require_once ('mail/class.smtp.php');
require_once('lib/DbModel.php');

// error_reporting(E_ALL ^ E_DEPRECATED);

function send_mail($to, $to_name, $subject, $message, $seconds,$attachments=array(),$cc=array()){
	set_time_limit($seconds);
	$db_model = new DbModel();
	$row = $db_model->first("select * from confmail where status = '1' order by id desc");
	// $num_row = mysql_num_rows($sql);
	// print_r($row);
	if($row){
		// while($row = mysql_fetch_array($sql)){
		// 	$host = $row['host'];
		// 	$fromID = $row['fromid'];
		// 	$fromName = $row['fromname'];
		// 	$login = $row['login'];
		// 	$password = $row['password'];
		// 	$port = $row['port'];
		// 	$smtpAuth = $row['smtpAuth'];
		// 	$smtpSecure = $row['smtpSecure'];
		// 	$smtpDebug = $row['smtpDebug'];
		// }
			$host = $row->host;
			$fromID = $row->fromid;
			$fromName = $row->fromname;
			$login = $row->login;
			$password = $row->password;
			$port = $row->port;
			$smtpAuth = $row->smtpAuth;
			$smtpSecure = $row->smtpSecure;
			$smtpDebug = $row->smtpDebug;
			
			$mail = new PHPMailer();
			$mail->IsSMTP();							// set mailer to use SMTP
			$mail->IsHTML(true);						// set HTML Is true to Execute HTML
			$mail->Host = $host;						// specify main and backup server
			$mail->SMTPAuth = $smtpAuth;				// turn on SMTP authentication
			$mail->Username = $login;					// SMTP username
			$mail->Password = $password;				// SMTP password
			$mail->Port = $port;
			$mail->SMTPSecure = $smtpSecure;			// SMTP Secure
			$mail->SetFrom($fromID, $fromName);			// SMTP From
			$count = count($to);
			for($i = 0; $i < $count; $i++){
				// echo $message[$i];
				if($to[$i] != ''){
					$mail->AddAddress($to[$i], $to_name[$i]);		// To Whom Mail Will be Send
					$mail->Subject = $subject[$i];					// Subject of the Mail
					$mail->Body  = $message[$i];
					// echo '---->'.'ddd';
					foreach($cc as $ccmail){
						$mail->addCC($ccmail);
					}				// Body of the Mail Content
					if(!empty($attachments)){
						foreach($attachments as $attachment){
							$base_path = __DIR__;
							// echo $base_path.'/'.$attachment;
							if(file_exists($base_path.'/'.$attachment)){
								// echo $base_path.'/'.$attachment;
								$mail->AddAttachment($base_path.'/'.$attachment);
							}
						}
					}
					if(!$mail->Send()){
					return "Mailer Error: " . $mail->ErrorInfo;
					exit;
					}
					else{
						return "Notification Mail Sent";	
												
					}
				}
			}		
	}
	else{
		return "Error : No Active Config Mail";
	}
}
?>
