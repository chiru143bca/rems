<?php
require_once('lib/DbModel.php');
require_once('lib/Utils.php');
$db_model = new DbModel();
$utils = new Utils();
$result = array("error"=>"error");
if($_POST){
    $mobile = $_POST['mobile'];
    $type = $_POST['type'];
    if($mobile && $type == 'forgot'){
        $check_mobile = $db_model->first("select * from users where mobile ='{$mobile}'");
        if($check_mobile && !empty($check_mobile)){
            // remove if prevoius reset password available
            $db_model->executeQuery("delete from password_resets where mobile = '{$mobile}'");
            $token = rand(10000,99999);
            $time = time();
            // insert token in password_resets table
            $db_model->insertData("insert into password_resets (token,mobile,created_at) values('{$token}','{$mobile}',$time)");
            // send sms to mobile
            $msg = "OTP to reset your password {$token}.";
            $res = $utils->sendSMS($mobile,$msg);
            if($res == 'error') $result = array('error'=>'Due to technical issues, password can\'t be reset now. Kindly try later.');
            else $result = array("success"=>"OTP sent successfully, Kindly change your password");
        }else{
            $result = array('error'=>'This mobile number is not registered with us, kindly provide registered mobile number.');
        }

    }else if($mobile && $type == 'reset'){
        $otp = $_POST['otp'];
        $password = $_POST['password'];
        // check otp
        $check_otp = $db_model->first("select * from password_resets where token = '{$otp}' and mobile= '{$mobile}'");
        if($check_otp && !empty($check_otp)){
            // change user table password
            $password = sha1($password);
            $update_sql = "update users set password='{$password}' where mobile = '{$mobile}'";
            $db_model->executeQuery($update_sql);
            $db_model->executeQuery("delete from password_resets where token = '{$otp}' and mobile = '{$mobile}'");
            $result = array("success"=>"Password Successfully updated.");
        }else{
            $result = array("error"=>"OTP got expired");
        }
    }
}
echo json_encode($result);
exit;