<?php
if(isset($_POST['excel'])){
    require_once('searchbuilder_new.php');
    require_once('lib/DbModel.php');
    require_once('lib/Utils.php');
    require_once('lib/AppView.php');
$db_model = new DbModel();
$utils = new Utils();
$appV = new AppView();
$tbl = ($_GET['page'] == 'reports')? 'orders': $_GET['page'];
    $qual_orig=" 1=1";
    $qual=isset($_POST['qual'])?$_POST['qual']:$qual_orig;
    $qual = $utils->extractDateFromQual($qual);
    $finalQual =  $utils->valueQual('reports' ,$_SESSION['SESS_access']);
    if(trim($qual)){
        if(trim($finalQual)){
            $finalQual .= ' and '.$qual;
        }else{
            $finalQual = $qual;
        }
    } 
    // echo $finalQual;exit;
    $field_show = array();
    // $fields = $utils->getNewTblQualWorkflow($tbl,false);
    // if($fields['fieldshow']) $field_show = explode(',',$fields['fieldshow']);
    $sku_masters = $utils->allArray("select distinct(sku_code),description from sku_master where status =1");
   
    $skus = array_column($sku_masters,'description');
    $sku_code = array_column($sku_masters,'sku_code');
    $fields = $utils->getTblRawFields($tbl);
    $field_show = $fields['name'];
    $alias =  array(
        "S.No",
        'Order No',
        'Order Date',
        'Customer Code',
        'Customer Name',
        'District',
        'Date of RTGS',
        'Cheque Number/UTR No.',
        'Paid Amount',
        'Virtual Code',
        'Bank Name');
       $alias2 = array(
        'Total Qty',
        'Order Value',
        'TCS Amount',
        'Final Value',
        'Source',
        'Status',
        'Dispatch Date',
        'Completion date',
        'Turnaround time',
        'Order creation date and time',
        'Order creation user',
        'Payment entry date & time',
        'Payment entry user',
        'Accounts confirmation date & time',
        'Accounts confirmation user',
        'Dispatch entry date & time',
        'Dispatch entry user',
        'Completion entry date & time',
        'Completion entry user'

    );
    $alias= array_merge($alias,$skus,$alias2);
    // $alias = array_values($fields['alias']);
    // $qual=$utils->getCustomerMapQual($qual);
    // echo $finalQual;
    $array_data = $appV->display_raw($tbl,$finalQual,1,$field_show);
    // print_r($array_data);exit;
    $data = array_values($array_data);
    $finaldata = array();
    $total_amount = 0;
    $sku_qty = array();
    foreach($data as $key=>$value){
        $customer = $utils->getTableObject('customers',$value['customer_id']);
        $bank = $utils->getTableObject('bank_master',$value['bank_id']);
        $source = $utils->getTableObject('source_master',$value['source_id']);
        $status = $utils->getOptionAliasTblField('orders','status',$value['status']);
        $dteStart = new DateTime($value['payment_date']); 
        $dteEnd   = new DateTime($value['dispatch_date']);
        $diff_date = $dteStart->diff($dteEnd);
        $turnaround = (($value['payment_date'] && $value['dispatch_date'])) ? $diff_date->format("%d day(s), %H hour(s), %I minute(s)") : '';
        $created_by = $utils->getTableObject('users',$value['created_by']);
        $confirmed_by = $utils->getTableObject('users',$value['confirmed_by']);
        $dispatched_by = $utils->getTableObject('users',$value['dispatched_by']);
        $unloaded_by = $utils->getTableObject('users',$value['unloaded_by']);
        $total_amount = $value['payment_amount'] + $total_amount;
        $sku_data = array();
        foreach($sku_code as $sku){
           $sku_data[] = $utils->getOrderDetailsBySKU($sku,$value['order_no']);
           if(isset($sku_qty[$sku])){
               $sku_qty[$sku] = $sku_qty[$sku] + $utils->getOrderDetailsBySKU($sku,$value['order_no']);
           }else{
                $sku_qty[$sku] = $utils->getOrderDetailsBySKU($sku,$value['order_no']);
           }    
        }
        $finaldata1 = array(
            $key+1, // S.No
            $value['order_no'], // Order No
            $value['order_date'], // Order Date
            $customer->customer_no, //Customer Code
            $customer->customer_name1, //Customer Name
            $customer->district, //District
            $value['payment_date'], //Date of RTGS
            $value['Payment_id'], //Cheque Number/UTR No.
            $value['payment_amount'], //Paid Amount
            $customer->virtual_code, //Virtual Code
            $bank->name_bank, //Bank Name
        );
        $finaldata2  = array(
            $value['total_quantity'], //Total Qty
            $value['amount_value'], //Order Value
            $value['tcs'], //TCS Amount
            $value['final_value'], //Final Value
            $source->source_short_desc, //Source
            $status, //Status
            $value['dispatched_time'], //Dispatch Date
            $value['unload_date'], //Completion date
            $turnaround, //Turnaround time
            $value['created_at'], //Order creation date and time
            $created_by->empname, //Order creation user
            $value['payment_date'], //Payment entry date & time
            $created_by->empname, //Payment entry user
            $value['confirmed_time'], //Accounts confirmation date & time
            $confirmed_by->empname, //Accounts confirmation user
            $value['dispatch_date'], //Despatch entry date & time
            $dispatched_by->empname, //Despatch entry user
            $value['unload_date'], //Completion entry date & time
            $unloaded_by->empname, //Completion entry user
        );
        $finaldata[] = array_merge($finaldata1,$sku_data,$finaldata2);
    }
    // last entry
    $finaldata1 = array('','','','','','','','',$total_amount,'','');
    $sku_totqty = array_values($sku_qty);
    $finaldata[] = array_merge($finaldata1,$sku_totqty);
    require_once 'plugin/phpoffice/excel/Classes/PHPExcel.php';
    $objPHPExcel = new PHPExcel();
    // Set document properties
    $objPHPExcel->getProperties()->setCreator("UB")
    ->setLastModifiedBy("UB")
    ->setTitle("UB OrderManagment")
    ->setSubject("UB OrderManagment")
    ->setDescription("UB OrderManagment")
    ->setKeywords("UB OrderManagment")
    ->setCategory("Orders");
    $objPHPExcel->setActiveSheetIndex(0)
            ->fromArray(
                $alias,
                NULL,
                'A1'
            );
    $objPHPExcel->setActiveSheetIndex(0)
            ->fromArray(
                $finaldata,
                NULL,
                'A2'
            );
    $styleArray = array(
        'font' => array(
            'bold' => true,
        )
    );
    $objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:ZZ1')->applyFromArray($styleArray);
// Miscellaneous glyphs, UTF-8



// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Transactions');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
$objPHPExcel = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$filename = 'public/transaction-details_'.date('d-M-y-H-m').'.xlsx';
$objPHPExcel->save($filename);
// header('Content-Type: application/vnd.ms-excel');
// echo json_encode(array("success"=>"test"));
echo $filename;
    // print_r($array_data);
    // exit;
}