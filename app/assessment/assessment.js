var option = {
        "rules":{
            "feedback":{
                "required":"Please priovide Feedback"
            },
            "start_time":{"required":"Please provide Start Time"
            },
            "end_time":{"required":"Please provide End Time"
            },
            "sched_date":{"required":"Please provide End Time"
            },
            "overall_score":{"required":"Please provide Overall score",
            "positvie_number":"Overall score must be positive number"
            }
        }
    };

$(function(){
    $('#area0').change(changeArea);
    $('button[name=updates]').click(function(){
        $("button[name=updates]").prop('disabled',true);
        var msg = $("#frm1").validate(option,0);
        if(msg){
            customAlert(msg,function(){});
            $("button[name=updates]").prop('disabled',false);
            return false;
        }
        $("button[name=updates]").prop('disabled',false);
    });
    $('#content-view').on('show.bs.modal',function(e){ contentShow(e); });
    $('button[name=alBtn]').click(function(){
        var that = this;
        $("button[name=updates]").prop('disabled',true);
        var msg = $("#frm1").validate(option,0);
        if(msg){
            customAlert(msg,function(){});
            $("button[name=updates]").prop('disabled',false);
            return false;
        }
        var status = $(that).attr('value');
        if(status == '2' || status == '4'){
            var cnt= $('#realtedForm #comments table tr.new').length;
            if(!cnt){
             customAlert('Please drop the comments before taking this action',function(){});
                return false;
            }
         }
        successAlert("Updated successfully.", function(){
            $('#frm1').append("<input name='status0' value='"+status+"' type='hidden'>");
            $('#frm1').append("<input name='updates' value='update' type='hidden'>");
            $('#frm1').submit();
        });
    });
    $('#jobreq_id0').change(showJobName);
    showJobName();
});

function changeArea(){
    var area = $(this).val();
    if(area){
        var url = 'assessment-template.php';
       var that = $("button[type=submit]")[0];
        var data = {area:area};
        ajaxCall(url,data,that,function(res){
            if(res.success){
                tinymce.get('feedback0').setContent(res.success);
            }else{
                inymce.get('feedback0').setContent('');
            }
        });
    }
}
function contentShow(e){
    that = $(e.relatedTarget);
    var ref= that.data('ref');
    var id = $("input[name=id0]").val();
    var title = that.data('title');
    var data ={id:id,ref:ref};
    var url = "assessment-data.php";
    that = $("input[type=submits]");
    ajaxCall(url,data,that,function(res){
        $('#content-preview').html(res.data);
        $('#content-view-label').text(title);
    });
}

function showJobName(){
    var req_id = $("#jobreq_id0");
    if(req_id.length){
        req_id = req_id.val();
        if(req_id.trim()){
            var url = "candidate-data.php";
            that = $('#btns');
            data = {req_id:req_id,id:req_id};
            ajaxCall(url,data,that,function(res){
                $('.jobname').text(res.data);
            });
        }
    }
}


