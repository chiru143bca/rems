var option = {"rules":{"input_date":{"required":"Please priovide Date"},"area":{"required":"Please provide area"},"skill":{"required":"Please provide Skill"},"candidate_name":{"required":"Please provide Candidate name"},"exp":{"positvie_number":"Eperience should be positive number"},"ctc":{"positvie_number":"CTC should be positive number"},"ectc":{"required":"Please provide Expected CTC","positvie_number":"Expected CTC should be positive number"},"notice":{"positvie_number":"Notice Peroid should be positive number"},"curr_loc":{"required":"Please provide Current Location"},"pref_loc":{"required":"Please provide Preferred Location"},"education":{"required":"Please provide Education"},"contact":{"required":"Please provide Contact"},"email_id":{"required":"Please provide Email ID"},"jobreq_id":{"required":"Please select Requisition number"}}};
$(function(){
    $('.status-cls').click(UpdateStatus);
    $('#childviewdiv').on("change","select[name=band0],select[name=template0]",ctcCalculation);
    $('#childviewdiv').on("keyup","input[name=ctc0]",ctcCalculation);
    $('#candidate-sendmail-modal,#candidate-gen-offfer-modal').on('show.bs.modal',function(){
        // add ctc at modal
        PopulateCTC(); 
    });
    $('#candidate-sendmail-modal').on("change","input[name=ctc_radio],#sendmail_email_template",loadCTCTemplate);
    $('#candidate-gen-offfer-modal').on("change","input[name=offer_ctc_radio],input[name=offer_template]",loadOfferTemplate);
    $("input[name=addrow]").click(AddRow);
    $('#childModal').on('change','select[name=area0]',changeArea);
    $('#content-view').on('show.bs.modal',function(e){ contentShow(e); });
    $('select[name=area0]').change(ChangeSubArea);
    $("#childModal").on('shown.bs.modal',function(e){ autoPopulateArea(e) });
    $('#jobreq_id0').change(showJobName);
    showJobName();
});
function AddRow(){
    $("input[name=addrow]").prop('disabled',true);
    var msg = $("#frm1").validate(option,0);
    if(msg){
        customAlert(msg,function(){});
        $("input[name=addrow]").prop('disabled',false);
        return false;
    }
    customAlert('Candidate data saved successfully',function(){
        $("#frm1").append("<input name='addrow' value='Add Row' type='hidden'>");  
        $('#frm1').submit();
    });  
}
function UpdateStatus(){
    var that = $(this);
    var status = that.val();
    var msg = $("#frm1").validate(option,0);
    if(msg){
        customAlert(msg,function(){});
        return false;
    }
    var successmessage = {2:"Candidate selected successfully",3:"Candidate rejected successfully",4:"Status changed to accepted offer successfully",5:"Status changed to offer declined successfully",6:"Status changed to joined successfully",7:"Status updated successfully",8:"Status updated successfully"};
    if(status == '2' || status == '3' || status == '4' || status == '5' || status == '6' || status == '7' || status == '8'){
        if(status == '3' || status == '5'){
           var cnt= $('#realtedForm #comments table tr.new').length;
           if(!cnt){
            customAlert('Please drop the comments before taking this action',function(){});
               return false;
           }
        }
        customAlert(successmessage[status],function(){

            $("#frm1").append("<input type='hidden' value='"+status+"' name='status0'>");
            $("#frm1").append("<input type='hidden' value='update' name='updates'>");
            $('#frm1').submit();
        });
    }else if(status == '1' || status == 'send mail'){
        $('#sendmail_share-btn').attr('data-status',status);
        $('#candidate-sendmail-modal').modal('show');
    }else if(status == '9'){
        $('#candidate-gen-offfer-modal').modal('show');
    }
    // console.log(status);
}

function sendMail(that){
    var email = $('input[name=sendmail_email]').val();
    var cc = $('input[name=sendmail_cc]').val();
    var msg = tinymce.get('sendmail_msg').getContent();
    var attachments = $('input[name=ctc_attachment]:checked').val();
    var id = $("input[name=id0]").val();
    var subject = $("#sendmail_subject").val();

    var required_fields = {email:[email,"Email Address"],subject:[subject,"Subject"],msg:[msg,"Message Content"]};
      var alert_msg ="";
      var flag = false;
      $.each(required_fields,function(k,v){
          if(!v[0]){
              alert_msg += "The "+v[1]+" is required!.<br>";
              flag = true;
          }
      });
      if(flag){
          customAlert(alert_msg,function(){});
          return false;
      }
    var attch = new Array();
    if(attachments) attch.push(attachments);
    var attachments = $("input[name='attachments[]']:checked");
    for(var i=0; i< attachments.length; i++){
        attch.push($(attachments[i]).val());
    }
    attch = attch.join(',');
    var data = {id:id,email:email,cc:cc,msg:msg,subject:subject,attachments:attch};
    var url = 'candidate-sendmail-ajax.php';
    ajaxCall(url,data,that,function(res){
        successAlert("Mail sent Successfully.",function(){

            $('#candidate-sendmail-modal').modal('hide');
            if($(that).attr('data-status') == '1') $("#frm1").append("<input type='hidden' value='1' name='status0'>");
            $("#frm1").append("<input type='hidden' value='update' name='updates'>");
            $('#frm1').submit();
        });
    });
}

function ctcCalculation(){
    var band = $("#childModal select[name=band0]").val();
    var template = $("#childModal select[name=template0]").val();
    var ctc = $("#childModal input[name=ctc0]").val();
    var that = $('#childModal #insertbtn')[0];
    if(band && template && ctc){
        var data = {band:band,template:template,ctc:ctc};
        var url = 'candidate-ctc-calc.php';
        ajaxCall(url,data,that,function(res){
            $.each(res,function(k,v){
                $("#childModal input[name="+k+"0]").val(v);
            });
        });
    }
}

function PopulateCTC(){
    var list = $("div#candidate_det table tr");
    var attachments = $("div#attachments table tr");
    var radio = "";
    var radio_offer = "";
    var attach = "";
    if(list.length > 2){
        $.each(list,function(k,v){
            if(k>0){
                var id = $(v).find("td:first span").attr('data-id');
                var ctc = $(v).find("td:nth-child(2)").text();
                if(ctc){

                    radio += '<label class="radio-inline">'+
                    '<input type="radio" name="ctc_radio" value="'+id+'"> '+ctc+
                    '</label>';
                    radio_offer += '<label class="radio-inline">'+
                    '<input type="radio" name="offer_ctc_radio" value="'+id+'"> '+ctc+
                    '</label>';
                }
            }
        });
    }
    if(attachments.length >2){
        $.each(attachments,function(k,v){
            if(k>0){
                var id = $(v).find("td:first span").attr('data-id');
                var text = $(v).find("td:nth-child(2)").text();
                if(text){
                    attach += '<li><label class="radio-inline">'+
                    '<input type="radio" name="ctc_attachment" value="'+text+'"> '+text+
                    '</label></li>';
                }
            }
        });
    }
    if(radio){
        $('.ctc_radio').html("<p>CTC</p>"+radio);
        $('.offer_ctc_radio').html("<p>CTC</p>"+radio_offer);
        $('.ctc_radio,.offer_ctc_radio').show();
        $('.has_attachment').html(attach);
    }
    else $('.ctc_radio,.offer_ctc_radio').hide();

    // CHeck if email Field available the add email auto field
    var souremail = $("input[name=email_id0]");
    if(souremail.length){
        $('#sendmail_email').val(souremail.val());
    }
}

function loadCTCTemplate(){
    var radio = $('input[name=ctc_radio]:checked').val();
    var cur_val = $('#sendmail_email_template').val();
    var content = "";
        if(cur_val){
           if( $('.subj-'+cur_val).length) $('#sendmail_subject').val($('.subj-'+cur_val).html());
           content = $('.htmlcontent-'+cur_val).html();
        }
    if(radio){
        var data = {ctc:radio}; 
        var that = $('#childModal #insertbtn')[0];
        var url = 'candidate-sendmail-ajax.php';
        ajaxCall(url,data,that,function(res){
            if(res.success){
                content = content+'<div class="clearfix"></div>'+res.success;
                tinymce.get('sendmail_msg').setContent(content);
            }
        });
    }else{
        tinymce.get('sendmail_msg').setContent(content);
    }

}
function loadOfferTemplate(){
    var radio = $('input[name=offer_ctc_radio]:checked').val();
    var offer_template = $('input[name=offer_template]:checked').val();
    if(radio && offer_template){
        var data = {offer_ctc:radio,offer_template:offer_template}; 
        var that = $('#childModal #insertbtn')[0];
        var url = 'candidate-sendmail-ajax.php';
        ajaxCall(url,data,that,function(res){
            if(res.success){
                tinymce.get('offer_content').setContent(res.success);
            }
        });
    }
}
function previewOffer(){
    var content = tinymce.get("offer_content").getContent();
    var id = $("input[name=id0]").val();
    var ctc = $('input[name=offer_ctc_radio]:checked').val();
    var data = {preview:content,id:id,ctc_preview:ctc};
    var that = $('#candidate-gen-offfer-modal #preview-offer-btn')[0];
    var url = 'candidate-sendmail-ajax.php';
    ajaxCall(url,data,that,function(res){
        if(res.success){
            // tinymce.get('offer_content').setContent(res.success);
            $('#offer-preview').html(res.success);
            $('#candidate-prev-offfer-modal').modal('show');
        }
    });

}
function saveOffer(){
    var id = $("input[name=id0]").val();
    var radio = $('input[name=offer_ctc_radio]:checked').val();
    var content = tinymce.get('offer_content').getContent();
    var url = "candidate-gen-offser.php";
    var data = {id:id,offer_content:content,ctc:radio};
    var that = $('#childModal #save-offer-btn')[0];
    ajaxCall(url,data,that,function(res){
        if(res.success){
            $("#attachments table tbody").append(res.success);
            $('#candidate-gen-offfer-modal').modal('hide');
        }
    });


}

function changeArea(){
    var area = $(this).val();
    if(area){
        var url = 'assessment-template.php';
       var that = $("button[type=submit]")[0];
        var data = {area:area};
        ajaxCall(url,data,that,function(res){
            if(res.success){
                tinymce.get('feedback0').setContent(res.success);
            }else{
                inymce.get('feedback0').setContent('');
            }
        });
    }
}

function contentShow(e){
    that = $(e.relatedTarget);
    var ref= that.data('ref');
    var id = $("select[name=jobreq_id0]").val();
    if(!id){
        customAlert("please select Requisition", function(){});
        return false;
    }
    var title = that.data('title');
    var data ={id:id,ref:ref};
    var url = "candidate-data.php";
    that = $("input[type=submits]");
    ajaxCall(url,data,that,function(res){
        $('#content-preview').html(res.data);
        $('#content-view-label').text(title);
    });
}

function ChangeSubArea(){
    var area = $('select[name=area0]').val();
    var url = 'candidate-data.php';
    var data = {area:area,id:''};
    that = $("input[type=submits]");
    if(area.trim()){
        ajaxCall(url,data,that,function(res){
            if(res.success){
                $("#sub_area0").html(res.data);
                if($("#frm1 #sub_area0").attr('data-selected')){
                    var sel = $("#frm1 #sub_area0").attr('data-selected');
                    $("#sub_area0").val(sel);
                    $("#frm1 #sub_area0").attr('data-selected','')
                }
            }
        });
    }
}

function autoPopulateArea(that){
    var btn = $(that.relatedTarget);
    var tbl = btn.data('table');
    if(tbl == 'assessment'){
       var area = $("#frm1 select[name=area0]").val();
       var subarea = $("#frm1 select[name=sub_area0]").val();
       var cand_name = $("#frm1 input[name=candidate_name0]").val();
       elementLoaded('#childModal select[name=area0]',function(){
            $("#childModal select[name=area0]").val(area);
            $("#childModal select[name=sub_area0]").val(subarea);
            $("#childModal select[name=area0]").change();
            $("#childModal input[name=candidate_name0]").val(cand_name);
       });
    }
}
function showJobName(){
    var req_id = $("#jobreq_id0");
    var area_id = $('#frm1 #area0');
    if(req_id.length && !area_id.val()){
        req_id = req_id.val();
        if(req_id.trim()){
            var url = "candidate-data.php";
            that = $('#btns');
            data = {req_id:req_id,id:req_id};
            ajaxCall(url,data,that,function(res){
                res = res.data;
                $('.jobname').text(res.job_name);
                $('#frm1 #area0').val(res.area);
                $('#frm1 #area0').change();
                $('#frm1 #sub_area0').attr('data-selected',res.sub_area);
                $('#frm1 #job_type0').val(res.job_type);
            });
        }
    }
}
function elementLoaded(el, cb) {
    if ($(el).length) {
      cb($(el));
    } else {
      // Repeat every 500ms.
      setTimeout(function() {
        elementLoaded(el, cb)
      }, 500);
    }
  };