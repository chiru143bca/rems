<?php
session_start();
require_once('lib/DbModel.php');
$db_model = new DbModel();
$tr ="";
if($_POST){
    if(isset($_POST['comment']) ){
        $comment = $_POST['comment'];
        $order_id = $_POST['order_id'];
        $file= isset($_FILES['file'])? $_FILES['file'] : array();
        $time = time();
        $user_id = $_SESSION['SESS_id'];
        $query = "insert into comments (created_at,created_by,description,user_id,orderno,attachments,file_name) values($time,$user_id,'{$comment}',$user_id,$order_id,";
        if(!empty($file)){
            $filename =$file['name'];
            $filepath = 'public/comments/'.$order_id.'_'.time().$filename;
            move_uploaded_file($file['tmp_name'],$filepath);
            $query .= " '{$filepath}','{$filename}' )";
        }else{
            $query .= " '','' )";
        }
        $id = $db_model->insertData($query,true);
        // echo $query;
        // echo "select * from comments where id = {$id}";
        $sql1="select comments.user_id,comments.description,comments.created_at,comments.attachments,comments.file_name,users.empname,users.id from users,comments where ( comments.id = {$id} and users.id = comments.user_id )  ";
        // echo $sql1;
        $data = $db_model->first($sql1);
        // $data = $db_model->first("select * from comments where id = {$id}");
        $data->created_at = date('d-m-Y H:i',$data->created_at);
        $file ="";
        if($data->attachments){
            $file = "<a href=\"".$data->attachments."\" download>".$data->file_name."</a>";
        }
        $tr = "<tr class=\"new\"><td>".$data->empname."</td><td>".$data->created_at."</td><td>".$data->description."</td><td>".$file."</td></tr>";
    }
}
echo json_encode(array('success'=>$tr));
exit;