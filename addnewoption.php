<?php
session_start();
   require_once('lib/Utils.php');
   $utils = new Utils();
if(isset($_POST['destination'])){
    $cur_table = $_POST['cur_tbl'];
    $destination = $_POST['destination'];
    $field  = $_POST['field'];
    $showfields = $_POST['showfields'];
    $post_showfields = explode(',',$showfields);
    $vals = "";
    $values = array();
    foreach($post_showfields as $field_name){
        $value = isset($_POST[$field_name]) ? "'".addslashes($_POST[$field_name])."'" : "''";
        array_push($values,$value);
    }
    $vals = implode(',',$values);
    $insert_query = "insert into {$destination} ($showfields) values ({$vals})";
    $last_id = $utils->insertData($insert_query,true);
    $option = $utils->getListOptions($cur_table,$field,$last_id);
    echo json_encode(array('success'=>"success",'res'=>$option));
    exit;
}