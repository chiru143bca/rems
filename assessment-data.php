<?php
session_start();
require_once('lib/CoreView.php');
require_once('lib/Utils.php');
$coreV = new CoreView();
$utils = new Utils();
$user_id = $_SESSION['SESS_id'];
$id = $_POST['id'];
$parent_id = $utils->getParentRecordId('candidates','assessment','sel='.$id);
if($_POST['ref'] == 'assessment'){
    $reldata = $utils->first("select id from formrel where parent = 'candidates' and child = 'assessment'");
    $rel_id = $reldata->id;
    $sql = "select * from assessment where id in (select childrecid from reldata where parentrecid = '$parent_id' and relid='{$rel_id}' and childrecid != '{$id}')";
    // echo $sql;
    $alldata = $utils->all($sql);
    $data = "<table class='table'><thead><tr><th>Panel</th><th>Interview type</th><th>Overall score</th><th>Feedback</th><th>Date</th></tr></thead><tbody>";
    foreach($alldata as $ind){
        $data .= "<tr><td>".$utils->getAlias('assessment','panel',$ind->panel)."</td>";
        $data .= "<td>".$utils->getAlias('assessment','interview_type',$ind->interview_type)."</td>";
        $data .= "<td>".$ind->overall_score."</td>";
        $data .= "<td>".$ind->feedback."</td>";
        $data .= "<td>".$utils->getmydate($ind->sched_date)."</td></tr>";
    }
    $data .= "</tbody></table>";

}else if($_POST['ref'] == 'content'){
    $sql = "select cv_content from candidates where id = '{$parent_id}'";
    $alldata = $utils->first($sql);
    $data = $alldata->cv_content;
}

echo json_encode(array('success'=>'success','data'=>$data));
exit;