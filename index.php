<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>InputZero Technologies Pvt. Ltd</title>
	<link rel="stylesheet" type="text/css" href="css/login.css" />
	<link href="css/bootstrap.min.css" rel="stylesheet"/>
	<link href="css/style.css" rel="stylesheet"/>
</head>

<body>
<center>
    <div class="container-fluid">
    	<div id="top_left">
			
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<br/><br/><br/><br/>
                </div>
	</div>
	<div class="row" >
		<div class="col-md-4 col-sm-12">
		</div>
		<div id="121" class="col-md-4 col-sm-12">
			<form role="form" method="POST" action="check_login.php" class="modal-content">
				<div class="textblock">	
					<img id="img" src="images/logo.png" alt="UB-procurment" />
					<div class="ubheader">
						<h3><center>  InputZero Technologies Pvt. Ltd </center><div style="height:5px;"></div><center style="font-size:15px;">ATS</center></h3>
					</div>
					<?php if(isset($_GET['msg']) && $_GET['msg']){ ?>
					<div class="alert alert-danger">
					<?php echo $_GET['msg']; ?>
					</div>
					<?php } ?>
					<div class="maincntn">
						<hr />

						<h5><strong>LOGIN</strong></h5>
						<div class="form-group">					 
							<input class="form-control" type="text" placeholder="Username" name="myusername" id="myusername" style="line-height: 10px;" required>
						</div>
						<div class="form-group">					 
							<input class="form-control" name="mypassword" type="password" id="mypassword" style="line-height: 10px;" placeholder="Password" required>
						</div>
						<div><button type="submit" class="btn btn-primary">
								Login
							</button>
							
						</div>
						<div>
							<br />
							<a id='btn' name='forgot_pass' href="forgotpassword.php">Forgot / Reset Password</a>
						</div>
						<div>
						<!-- <input id='btn' class="btn btn-primary" type='button' name='ub_user' value='Login as UB User' onclick="location.href='module.php/core/authenticate.php?as=ub-user'"/> -->
						
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="col-md-4">
		</div>
	</div>
	<div id="footer">
		<strong>
			&copy; InputZero Technologies
		</strong>
	</div>
</center>
</body>
</html>
