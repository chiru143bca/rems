<?php
require_once('lib/triggers.php');
if($_POST)
{
    // print_r($_POST);
    if(isset($_POST['tableid']) && !empty($_POST['tableid']))
        $tblid=$_POST['tableid'];
    // if(isset($_POST['fieldid']) && !empty($_POST['fieldid']))
    //     $fieldids=$_POST['fieldid'];
    if(isset($_POST['updateid']) && !empty($_POST['updateid']))
    {    
        $updateids=$_POST['updateid'];
    }    
    if(!empty($tblid))
    {
        if(isset($updateids) && $updateids)
        {
            $selfields=$updateids;
            $show_query=mysql_query("select logshow from activitylogconfig where tblid='$tblid'") or die(mysql_error());
            if(mysql_num_rows($show_query))
            {
                
                $logshow=mysql_result($show_query,0);
            }
            else
            {
                $logshow=1;
            }
            $del_query="delete from activitylogconfig where tblid='$tblid'";
            $res=mysql_query($del_query);
            foreach(array_keys($selfields) as $selfield)
            {
                $ins="insert into activitylogconfig(tblid,fieldid,trigger_status,logshow) values('$tblid','$selfield',1,'$logshow')";
                $result=mysql_query($ins) or die(mysql_error());
                // echo $result;
            }
            $q="select name from config where tblid='$tblid'";
            $res=mysql_query($q) or die(mysql_error());
            $row=mysql_fetch_assoc($res);
            $tblname=$row['name'];
            
        }
        else
        {
            // $q=mysql_query("drop trigger if exists after_update_trigger") or die("drop trigger query not executed".mysql_error());

            $del_query="delete from activitylogconfig where tblid='$tblid'";
            $res=mysql_query($del_query);
        }

    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Activity Log</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
<div id="div2"></div>
    <form>
    <div class="form-group">
        <?php 
            $tbl_query="select * from config where alias not in ('config','field','field_option','access','groups','Users','Events','valuelist')";
            $result=mysql_query($tbl_query) or die(mysql_error());
            $rowscount=mysql_num_rows($result);
        ?>
        <h3>Select Table for Activity Log </h3>
        <label for="tableid">Table </label>
        <select name="tableid"  id="table">
            <option value="">Select Table</option>
        <?php
            if($rowscount > 0)
            {
                while($row=mysql_fetch_assoc($result))
                {
                    echo '<option value="'.$row['tblid'].'">'.$row['alias'].'</option>';

                }
            }
            else
                echo '<option value="">Table Not Available</option>';

        ?>
        </select>
          
        <br><br>    
        <!-- <select name="fieldid[]"  id="field" multiple="multiple">
            <option value="">Select Table First</option>
        </select> -->
        <ul class="list-group col-md-4" name="fieldid[]"  id="field" style="list-style-type:none" >
        </ul>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <hr>
    <?php
    $triggered_tbls_query="select distinct(tblid) from activitylogconfig";
    // $triggered_tbls_query="select tblid from config";
    $res=mysql_query($triggered_tbls_query) or die(mysql_error());
    $acttbls=array();
    while($row=mysql_fetch_assoc($res))
    {
        array_push($acttbls,$row['tblid']);
    }
    // print_r($_POST);
    echo "<h4>Activity Log</h4>";
    echo "(switch on  - Enables Activity Log on Form,&nbsp";
    echo "switch off - Disables Activity Log on Form)";
    echo "<form name=\"form1\" action=\"act.php\" method=\"POST\">";    
    foreach( $acttbls as $acttblid)
    {
    $tbl_name_query="select name,alias from config where tblid='$acttblid'";
    $re=mysql_query($tbl_name_query) or die(mysql_error());
    
    echo "<ul class=\"list-group  \" style=\"list-style-type:none\">";
    
    while($row=mysql_fetch_assoc($re))
    
    {
        // $xx=[];
        // $qq="select distinct(tblid) from activitylogconfig";
        // $res=mysql_query($qq) or die(mysql_error());
        // while($row=mysql_fetch_assoc($res))
        // {
        //     array_push($xx,$row['tblid']);
        // }
        // if(in_array($acttblid,$xx))
        // {
            $q="select logshow from activitylogconfig where tblid='$acttblid'";
            $res=mysql_query($q) or die(mysql_error()); 
            $resu=mysql_result($res,0);
            if($resu==1)
            $checked="checked";
            else
            $checked="";
        // }
        // else
        //     $checked=""; 

            // echo "<li class=\"list-group-item\">"."<input type=\"checkbox\"".$checked." name=\"tblid[$id]\"   onchange=\"change_visibility()\">".$row['alias']."</li>";
        echo "<li class=\"list-group-item\">".
       // "<input type=\"checkbox\"".$checked." name=\"tblid[$acttblid]\"   onchange=\"change_visibility()\">".
        "<label class=\"switch\"><input type=\"checkbox\"".$checked." name=\"tblid[$acttblid]\" data-rowid=\"$acttblid\" class=\"chk\" onchange=\"change_visibility()\"><span class=\"slider round\"></span></label>".
        "&nbsp&nbsp".$row['alias']."</li>";
    }
    echo "</ul>";
    }
    // echo "<button type=\"submit\" class=\"btn btn-default\">Submit</button>";
    echo "</form>";
    ?>
    <script type="text/javascript" >
    
        $(function(){
            var tbl;var fld;
            $('#table').on('change',function(){
                var tblid=$(this).val();
                tbl=tblid;
                if(tblid)
                {
                    
                    var request= $.ajax(
                        {
                            method:"POST",
                            url:"activitylogconfigajax.php",
                            data:{tblid:tblid},
                            dataType:"html"
                        })
                        request.done(function(msg){
                            $("#field").html(msg);  
                        });    
                }
                else
                {
                    $('#field').html('<option value="">Select Table First</option>');
                }
            });
            
            // $("#field").on('change',function(){
            //     var fieldid=$(this).val();
            //     fld=fieldid;
            //     if(fieldid)
            //     {
            //             var req2=$.ajax({
            //                 method:"POST",
            //                 url:"activitylogajax.php",
            //                 data:{fieldid:fieldid},
            //                 dataType:"html"
            //             })
            //             req2.done(function(msg2){
            //                 $("#div2").html(msg2); 
            //             });
            //     }
            //     alert(tbl);
            //     alert(fld); 
            // });
        });       
 $('.chk').change(function(){
    var rowid=$(this).attr('data-rowid');
    var status; 
    if($(this).is(":checked"))
        status="checked";
    else
        status="unchecked";
    var dbr_request=$.ajax(
                {
                    method:"POST",
                    url:"newactivitylogajax.php",
                    data:{rowid:rowid,chkstatus:status},
                    dataType:"html"
                })
                dbr_request.done(function(m){
                // console.log(m);
                });
});
    </script>
</body>
</html>