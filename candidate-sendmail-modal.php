<div class="modal fade" id="candidate-sendmail-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Email Info</h4>
      </div>
      <div class="modal-body">
          <div class="col-md-12">

              <div class="form-group">
                  <label for="search-name">Email Address</label>
                  <input type="text" class="form-control" name="sendmail_email" id="sendmail_email" placeholder="Email Address">
                </div>
                <div class="form-group">
                    <label for="search-name">CC :</label>
                    <input type="text" class="form-control" name="sendmail_cc" id="sendmail_cc" placeholder="Email Address">
                </div>
                <div class="form-group">
                    <label for="search-name">Attachments</label>
                    <ul class="has_attachment">

                    </ul>
                </div>
                <div class="form-group ctc_radio" style="display:none">
                  
                   
                </div>
                <div class="form-group">
                    <label for="search-name">Select Template:</label>
                    <select name="sendmail_email_template" id="sendmail_email_template">
                      <?php 
                        $template_sql = "select * from notification_templates";
                        $template_datas = $utils->all($template_sql);
                      ?>
                      <option value="">--Select Template--</option>
                      <?php foreach($template_datas as $template_data){ ?>
                        <option value="<?php echo $template_data->id ?>"><?php echo $template_data->template_name; ?></option>
                      <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="search-name">Subject</label>
                    <input name="sendmail_subject" id="sendmail_subject" class="form-control">
                </div>
                <div class="form-group">
                    <label for="search-name">Message Content</label>
                    <textarea name="sendmail_msg" id="sendmail_msg" class="ihtml" ></textarea>
                </div>
            </div>
        </div>
            <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-status='' id="sendmail_share-btn" onclick="return sendMail(this)">Share</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="candidate-gen-offfer-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Offer Generation</h4>
      </div>
      <div class="modal-body">
          <div class="col-md-12">

              
                <div class="form-group offer_ctc_radio" style="display:none">
                  
                   
                </div>
                <div class="form-group">
                    <p>Select Template:</p>
                      <?php 
                        $offer_template_sql = "select * from letter_templates";
                        $offer_templates = $utils->all($offer_template_sql);
                        ?>
                      <?php foreach($offer_templates as $offer_template){ ?>
                        <label class="radio-inline"><input type="radio" name="offer_template" value="<?php echo $offer_template->id ?>"> <?php echo $offer_template->template_name; ?></label>
                      <?php } ?>
                </div>
               
                <div class="form-group">
                    <label for="search-name">Offer Content</label>
                    <textarea name="offer_content" id="offer_content" class="ihtml" ></textarea>
                </div>
            </div>
        </div>
            <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="preview-offer-btn" onclick="return previewOffer()">Preview</button>&nbsp;
        <button type="button" class="btn btn-primary" id="save-offer-btn" onclick="return saveOffer()">Save Offer</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="candidate-prev-offfer-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="candidate-prev-offfer-modal-label">Offer Preview</h4>
      </div>
      <div class="modal-body">
          <div class="col-md-12">
              <div id="offer-preview"></div>
              
                
            </div>
        </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="content-view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="content-view-label"></h4>
      </div>
      <div class="modal-body">
          <div class="col-md-12">
              <div id="content-preview"></div>
              
                
            </div>
        </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<span class="hide">
  <ul>

    <?php foreach($template_datas as $template_data){ 
      echo "<li class='htmlcontent-{$template_data->id}'>{$template_data->htmlcontent}</li>";
      echo "<li class='subj-{$template_data->id}'>{$template_data->subject}</li>";
    }
    ?>
    </ul>

</span>