<?php
require_once('lib/DbModel.php');
require_once('lib/Utils.php');
function add_statusbar($tbl,$qual,$dates=NULL)
{
$db_model = new DbModel();
$utils = new Utils();
	$record_id=$qual;
    $div_cls="<div class=\"container\"><div class=\"row statbar\"><div class=\"col-md-12\" style=\"padding-left:0px\"><div style=\"display:inline-block;width:95%;overflow: auto;\">";
    $ul_pr="<ul class=\"timeline timeline-horizontal\">";
    $div_cls_cl="</div>";
    $ul_pr_cl="</ul>";
    $li="<li>";
	$li_act="<li class=\"timeline-item\">";
	$badge_open= "<div class=\"timeline-badge col-xs-1";
	$badge_div_cl= ">";
	$badge_cl = "</div>";
	$li_act_span = "<span style=\"position:absolute;top:50px;left:45px;width:1.1110\" >";
    $li_cl="</span></li>";
	$status_tbls=array();
	//added by suhela
	$img_array = array(0=>'sta_draft.png',1=>'sta_order.png',2=>'sta_sentback.png',3=>'sta_reorder.png',4=>'sta_confirm.png',5=>'sta_dispatch.png',6=>'sta_reject.png',7=>'sta_deliver.png',8=>'sta_cancel.png');
	$img_array = array(0=>'sta_draft.png',1=>'sta_order.png',2=>'sta_sentback.png',3=>'sta_reorder.png',4=>'sta_confirm.png',5=>'sta_dispatch.png',6=>'sta_reject.png',7=>'sta_deliver.png',8=>'sta_cancel.png');
	$img_tg="<img class=\"stimg\" src=\"images\\";
	$img_cl="\" height=\"30\" width=\"30\" style=\"margin-top:-10px;margin-left:-5px;\">";
	//closed by suhela
	$q1="select tblname from statusbar";
	$res=$db_model->allArray($q1);
	// while($row=mysql_fetch_assoc($res))
	foreach($res as $row)
	{
		array_push($status_tbls,$row['tblname']);
	}
	
	if(in_array($tbl,$status_tbls))
	{
		$fieldid_query="select fieldid,tblid,tblname,fieldname,visible from statusbar where tblname='$tbl'";
		$res=$db_model->allArray($fieldid_query);
		// while($row=mysql_fetch_assoc($res))
		foreach($res as $row)
		{
			
			$tblname=$row['tblname'];
			$tblid=$row['tblid'];
			$fieldid=$row['fieldid'];
			$fieldname=$row['fieldname'];
			$visible=$row['visible'];
		}
		
			if($visible==1)
			{ 
			echo $div_cls;
			echo $ul_pr;
			$status_query="select value,alias from field_option where tblid='$tblid' and fieldid='$fieldid'";
			$res1=$db_model->allArray($status_query);
			$status_list=array(); 
			foreach($res1 as $row)
			{
				$status_list[$row['value']]=$row['alias'];
			}
			
			$q="select $fieldname,modified_at from $tblname  where $record_id";
			$res=$db_model->allArray($q);
			$active_status='';
			foreach($res as $row)
			{
				if(isset($row[$fieldname]) && in_array($row[$fieldname],array_keys($status_list)))
				{

					$active_status=$status_list[$row[$fieldname]];
					$active_status_date=date("d-m-Y", $row['modified_at']);
				}
				else
					$active_status='';
			}
			$x=0;$a='';
			// Current table record
			$sql = "select * from {$tbl} where {$record_id} ";
			$record = $db_model->firstArray($sql);
			foreach($status_list as $key=>$value)
			{
				$status_date = "";
				$img = 'sta_draft.png';
				
				if(isset($dates[$key]) && isset($dates[$key]['field']) && $dates[$key]['field']){
					$field_date = isset($record[$dates[$key]['field']]) ? $utils->getmydate($record[$dates[$key]['field']]):"";
					$status_date = $field_date;
					$img = isset($dates[$key]['img']) ? $dates[$key]['img']:"";
				}
				if(isset($dates[$key]) && isset($dates[$key]['img'])){
					$img = isset($dates[$key]['img']) ? $dates[$key]['img']:"";
				}

				if($status_list[$key]==$active_status)
				{
					$x=1;
					$im_tg=$img_tg.$img.$img_cl;
					$li = $li_act.$badge_open;
					if($key == '3' || $key == '5'){
						$li .= " danger\"".$badge_div_cl.$im_tg.$badge_cl; 
					}
					else{
						if($key == '2'){
							$li .= " success\"".$badge_div_cl.$im_tg.$badge_cl;
						}
						else{
							$li .= " success\"".$badge_div_cl.$im_tg.$badge_cl;
						}
					}
					$a.= $li.$li_act_span.$status_list[$key]."<br>".$status_date.$li_cl;//T0 add date to active status
					
					// $a.= $status_list[$key]."<br>".'12-may-1202'.$li_cl;//T0 add date to active status
				}		
				else	
				{	
					if($x>0)
					{
						$x++;
						$im_tg=$img_tg.$img.$img_cl;
						$li = $li_act.$badge_open;
						$li .= " grey\"".$badge_div_cl.$im_tg.$badge_cl;
						$a.= $li.$li_act_span.$status_list[$key].$li_cl;//To add date to next statuses of active status
					}	
					else
					{
						if($x==0){
							$im_tg=$img_tg.$img.$img_cl;
							$li = $li_act.$badge_open;
							$li .= " \"".$badge_div_cl.$im_tg.$badge_cl;
							$a.= $li.$li_act_span.$status_list[$key]."<br>".$status_date.$li_cl;//To add date to previous statuses of active status
							 
						}

					}	

				}	
				
			}
			echo $a.$ul_pr_cl;
			echo $div_cls_cl.$div_cls_cl.$div_cls_cl.$div_cls_cl;
			echo "<div class='clearfix'>&nbsp;</div>";
			}
	}
 	 
}
?>