<?php
date_default_timezone_set("Asia/Kolkata");
require_once('Utils.php');
class RemsLib extends Utils{
    
    function candidateSequence($id){
        $sql = "select max(sequence_number) as squence from candidates";
        $data = $this->first($sql);
        $next_squence = $data->squence ? sprintf('%04d',$data->squence+1) : '0001';
        $candidate_no =  'CAND_'.date('Y').'_'.$next_squence;
        $update = "update candidates set sequence_number = '{$next_squence}', candidate_id ='{$candidate_no}' where id ='{$id}'";
        $this->executeQuery($update);
    }
    function requisitionSequence($id){
        $sql = "select max(sequence_number) as squence from job_details";
        $data = $this->first($sql);
        $next_squence = $data->squence ? sprintf('%04d',$data->squence+1) : '0001';
        $candidate_no =  'IZT_'.date('Y').'_'.$next_squence;
        $update = "update job_details set sequence_number = '{$next_squence}', job_id ='{$candidate_no}' where id ='{$id}'";
        $this->executeQuery($update);
    }

    
}