<?php
require_once('Utils.php');
require_once('UiConstant.php');
class SearchHistory{
    public $bl;
    function __construct($tbl){
        $this->tbl = $tbl;
    }
    function displayTails(){
        $tbl = $this->tbl;
        $utils = new Utils();
        $ui = new UiConstant();
        $sql = "select * from search_criteria where page = '{$tbl}'";
        $datas = $utils->all($sql);
        $div = $ui->tg_div.$ui->tg_div_class.'scrollmenu'.$ui->tg_cl;
        if($datas && !empty($datas)){
            foreach($datas as $data){
               $div .= "<a href='#' data-qual='".htmlspecialchars($data->criteria,ENT_QUOTES)."'>".$data->name."</a>";
            }
        }
        $div .= $ui->tg_div_cl;

        return $div;
    }
}