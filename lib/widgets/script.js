$(function(){
    var w_typr = $('#widget_type').val();
    if(w_typr == 1){
        $('#multiple-widget').addClass('hide');
    }else{
        $('#multiple-widget').removeClass('hide');
    }
    $('#widget-btn').click(function(){
        $('.widget-content').toggleClass('hide');
            $('#search-bar').collapse('toggle');
    });
    $('#filterbtn').click(function(){
        $('.widget-content').addClass('hide');
    });
    $('#search-bar').on('hide.bs.collapse',function(){
        $('.widget-content').addClass('hide');
    });
    $('#widget_type').change(function(){
        var curr = $(this).val();
        if(curr == 1){
            $('#multiple-widget').addClass('hide');
        }else{
            $('#multiple-widget').removeClass('hide');
        }
    });

    $('#save-widget').click(function(e){
        e.preventDefault();
        saveWidget(function(){

        });
    });

});

function saveWidget(cb){
    var url = $('#save-widget').attr('data-url');
    var all_data = $("#widgetmodal input,.widget-content input,.widget-content select,#qual_field");
    var required_fields = ["widget_label","widget_type","widget_field","widget_operator","widget_name","widget_label"];
    if($('#widget_type').val() == '2'){
        required_fields = ["widget_label","widget_type","widget_field","widget_operator","multi_delimit","multi_widget_label","multi_widget_field","multi_widget_operator","widget_name","widget_label"];
    }
    var alert_msg ="";
    var flag = false;
    var final_data = new Array();
    $.each(all_data,function(k,v){
       var name = $(v).attr('name');
       var value = $(v).val();
        final_data.push({name:name,value:value});
    });
    // $.each(all_data,function(k,v){
    //     if(!v[0]){
    //         alert_msg += "The "+v[1]+" is required!.<br>";
    //         flag = true;
    //     }
    // });
    // if(flag){
    //     customAlert(alert_msg,function(){});
    //     return false;
    // }
    $.ajax({
        type:'post',
        url:url,
        data:final_data,
        beforeSend:function(){
            $('#save-widget').prop('disabled',true);
        },
        complete: function(){
            $('#save-widget').prop('disabled',false);
        },
        success :function(res){
            $('#widgetmodal').modal('hide');
            $('#search-bar').collapse('hide');
            cb();
        }
    });
}