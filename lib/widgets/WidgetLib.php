<?php
require_once(__DIR__.'/../Utils.php');
class WidgetLib extends Utils{

    function getButton(){
        $btn = "<button type=\"button\" class='btn btn-info' id='widget-btn'>Widget</button>";
        return $btn;
    }

    function widgetContent(){
        $tbl = $_GET['page'];
        $content = "
            <div class=\"widget-content hide\">
            <input type='hidden' value='".$tbl."' name='widget_form'>
                <div class=\"col-md-12 border\">
                    <div class=\"col-md-2\">
                        <div class=\"form-group\">
                            <label for=\"widget_type\">Select Basic</label>
                            <select id=\"widget_type\" name=\"widget_type\" class=\"form-control col-md-2\">
                                <option value=\"1\">Single</option>
                                <option value=\"2\">Multiple</option>
                                <option value=\"3\">Chart</option>
                            </select>
                        </div>
                    </div>
                    <div class=\"clearfix\"></div>
                    <div class=\"col-md-3\">
                        <div class=\"form-group\">
                            <label for=\"widget_lbl\">Label</label>
                            <input type=\"text\" name='widget_lbl' class=\"form-control\" placeholder=\"Label\" id=\"widget_lbl\">
                        </div>
                    </div>
                    <div class=\"col-md-3\">
                        <div class=\"form-group\">
                        <label for=\"inputValue\">Fields</label>
                        <select id=\"widget_field\" name=\"widget_field\" class=\"form-control\">
                            ".$this->getFieldOptions()."
                        </select>
                        </div>
                    </div>
                    <div class=\"col-md-3\">
                        <div class=\"form-group\">
                        <label for=\"widget_operator\">Operator </label>
                        <select id=\"widget_operator\" name=\"widget_operator\" class=\"form-control\">
                            <option value=\"sum\">Sum</option>
                            <option value=\"avg\">Avg</option>
                            <option value=\"count\">Count</option>
                        </select>
                        </div>
                        </div>
                       <div class=\"clearfix\"></div>
                    <!--- Multiple widget --->
                    <div id=\"multiple-widget\">
                    <div class=\"col-md-3\">
                        <div class=\"form-group\">
                            <label for=\"multi_widget_delimit\">Delimit</label>
                            <input type=\"text\" name='multi_delimit' class=\"form-control\" placeholder=\"Delimit\" id=\"multi_delimit\">
                        </div>
                    </div>

                    <div class=\"col-md-3\">
                        <div class=\"form-group\">
                            <label for=\"multi_widget_lbl\">Label</label>
                            <input type=\"text\" name='multi_widget_lbl' class=\"form-control\" placeholder=\"Label\" id=\"multi_widget_lbl\">
                        </div>
                    </div>
                    <div class=\"col-md-3\">
                        <div class=\"form-group\">
                            <label for=\"multi_widget_type\">Fields</label>
                            <select id=\"multi_widget_field\" name=\"multi_widget_field\" class=\"form-control\">
                                ".$this->getFieldOptions()."
                            </select>
                        </div>
                    </div>
                    <div class=\"col-md-3\">
                        <div class=\"form-group\">
                            <label for=\"multi_operator\">Operator </label>
                            <select id=\"multi_widget_operator\" name=\"multi_widget_operator\" class=\"form-control\">
                                <option value=\"sum\">Sum</option>
                                <option value=\"avg\">Avg</option>
                                <option value=\"count\">Count</option>
                            </select>
                        </div>
                    </div>
                    
                </div>
                <div class=\"clearfix\"></div>
                <button type='button'  class=\"btn btn-info\" href='#' data-target='#widgetmodal' data-toggle='modal'>Save</button>
                </div>
                </div>
            <div class=\"clearfix\"></div>
        ";
        $content = $content.$this->getSaveModal();
        return $content;
    }

    function getFieldOptions(){
        $tbl = $_GET['page'];
        $fields = "select fieldid,name,alias,type from field where tblid in (select tblid from config where name = '{$tbl}') and name not in ('id','created_at','modified_at','modified_by','created_by')";
        $fields = $this->all($fields);
        $options = "";
        foreach($fields as $field){
            $options .="<option value='".$field->fieldid."'>".$field->alias."</option>";
        }
        return $options;
    }
    function getSaveModal(){
        $modal = "
        <div class=\"modal fade\" id=\"widgetmodal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"widgetModalLabel\" aria-hidden=\"true\">
            <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                    <h4 class=\"modal-title\"  >Widget Details</h4>
                </div>
                <div class=\"modal-body\" >
                    <div class=\"col-md-12\">
                        <div class=\"form-group\">
                            <label for=\"\">Widget Name</label>
                            <input type=\"text\" name=\"widget_name\" id=\"widget_name\" class=\"form-control\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"\">Widget Label</label>
                            <input type=\"text\" name=\"widget_label\" id=\"widget_label\" class=\"form-control\">
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\"  >Close</button>
                    <button type=\"button\" id=\"save-widget\" class=\"btn btn-primary\" data-url=\"lib/widgets/index.php\"  >Save</button>
                </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
            </div>
        ";
        return $modal;
    }

    function getAllWidgets($tbl,$qual=''){
        $sql = "select * from widget where form_name = '{$tbl}' and status = 1";
        $data = $this->all($sql);
        $widg = "";
        foreach($data as $value){
            $widg .= $this->setWidget($value,$qual);
        }
        if($widg){
            $widg="<div class=\"col-md-12\"><div class=\"row\">".$widg."</div></div>";
        }
        return $widg;

    }

    function setWidget($data,$qual){
        $num = rand(1,6);
        $ref = $data->filter;
  
        $div = "<div class=\"col-sm-3\">";
        $div .="<div class=\"panel panel-default\">";
        $div .='<div class="panel-body text-center">';
        $div .="<a href=\"#\"  data-ref=\"$ref\" class='tiles'>";
        $div .= "</br>";
        $div .="<h4  class=\"text-center\"><span>".$this->setLabel($data,$qual)."</span></h4>";
        $div .= "<br>";
        $div .="<div class='clearfix'></div>";
        $div .="</a></div>";
        $div .="<div class=\"panel-footer text-center\">".$data->widget_label;
        $div .="</div>";
        $div .="</div>";
        $div .="</div>";
        return $div;
    }

    function setLabel($data,$qual){
        $label = $data->label_name;
        $field_name = $data->field_name;
        $operator = $data->operator;
        $data->filter = ($qual) ? $data->filter.' and '.$qual: $data->filter;
        $display = "";
        if($data->widget_type == 1){
            $display = $label." ".$this->getValue($field_name,$operator,$data->filter,$data->form_name);
        }else if($data->widget_type == 2){
            $fields = explode(':',$field_name);
            $op = explode(':',$operator);
            $lbl = explode(':',$label);
            $val = array();
            foreach($fields as $key=>$value){
                $o = isset($op[$key])? $op[$key]:'';
                $list = $o." ".$this->getValue($value,$o,$data->filter,$data->form_name);
                array_push($val,$list);
            }
            $delimiter = $data->delimiter? $data->delimiter:' ';
            $display = implode('  '.$delimiter.'  ',$val);
        }else if($data->widget_type == 3){
            $display = $this->getChart();
        }

        return $display;
    }

    function getValue($field_id,$operator,$qual,$tbl){
        $field = $this->first("select name from field where fieldid = {$field_id}");
        $field = $field->name;
        if(trim($qual)) $qual = "where {$qual}";
        if($operator){
            $sql = "select $operator($field) as res from $tbl $qual";
            $data = $this->first($sql);
            if($data) return $data->res;
            else return '';
        }else{
            return '';
        }

    }

    function getChart(){
        $div ="<script>
        google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawAxisTickColors);

function drawAxisTickColors() {
      var data = google.visualization.arrayToDataTable([
        ['City', '2010 Population', '2000 Population'],
        ['New York City, NY', 8175000, 8008000],
        ['Los Angeles, CA', 3792000, 3694000],
        ['Chicago, IL', 2695000, 2896000],
        ['Houston, TX', 2099000, 1953000],
        ['Philadelphia, PA', 1526000, 1517000]
      ]);

      var options = {
        title: 'Population of Largest U.S. Cities',
        chartArea: {width: '50%'},
        hAxis: {
          title: 'Total Population',
          minValue: 0,
          textStyle: {
            bold: true,
            fontSize: 12,
            color: '#4d4d4d'
          },
          titleTextStyle: {
            bold: true,
            fontSize: 18,
            color: '#4d4d4d'
          }
        },
        vAxis: {
          title: 'City',
          textStyle: {
            fontSize: 14,
            bold: true,
            color: '#848484'
          },
          titleTextStyle: {
            fontSize: 14,
            bold: true,
            color: '#848484'
          }
        }
      };
      var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
        
        </script>";
        return $div;
    }
}