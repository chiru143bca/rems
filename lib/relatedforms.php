<?php
$parentid="";
$parenttbl="";
require_once('DbModel.php');
require_once('Utils.php');
function displayrelated($sel,$tbl,$related_btn= array(),$default='comments')
{
    $utils = new Utils();
    $db_model = new DbModel();
    $x=explode('=',$sel);
    $pid= $x[1];
    $parentid= $pid;
    $parenttbl=$tbl;
    $formreldata=array();
    $tbc="";$k=0;
    $qqq="select id,child from formrel where parent='$parenttbl'";
    $res=$db_model->allArray($qqq);
    // while($row=mysql_fetch_assoc($res))
    foreach($res as $row)
    {
        $formreldata[$row['id']]=$row['child'];
    }
    // <!-- Nav tabs -->
    //related forms button
    // $tb='<a class="btn btn-primary btn-xs" role="button" data-toggle="collapse" href="#realtedForm" aria-expanded="false" aria-controls="realtedForm">Related Forms </a><div class="clearfix">&nbsp;</div>';
    //removing relatedforms btn
    $tb='<a role="button" data-toggle="collapse" href="#realtedForm" aria-expanded="false" aria-controls="realtedForm" class="btn btn-primary">Related Table</a><div class="clearfix">&nbsp;</div>';

    $tb=$tb."<div class='in' id='realtedForm'>";
    $tb=$tb. "<ul class=\"nav nav-tabs\" role=\"tablist\">";
    $k=0;
    foreach($formreldata as $tbl)
    {
        $tbl_alias = $utils->getTableAlias($tbl);
        $ad=" class=\"active\"";$adc=" active";
        if($tbl== 'assessment'){
            $tb=$tb."<li role=\"$tbl\" $ad>". "<a href=\"#".$tbl."\" aria-controls=\"$tbl\" role=\"tab\" data-toggle=\"tab\">".$tbl_alias."</a></li>";
        }else{
            $tb=$tb."<li role=\"$tbl\">". "<a href=\"#".$tbl."\" aria-controls=\"$tbl\" role=\"tab\" data-toggle=\"tab\">".$tbl_alias."</a></li>";
        }

        $k++;
    }
    $tb=$tb. "</ul>";
    // <!-- Tab panes -->
    $tb=$tb."<div class=\"tab-content\">";
    //class=\"tab-pane active\"
    $k=0;
    // print_r($formreldata);
    foreach($formreldata as $id => $tbl)
    {
        $btn_new ="";
        $edit_flag = 0;
        $delete_flag = 0;
        if(isset($related_btn['edit']) && in_array($tbl,$related_btn['edit'])) $edit_flag = 1;
        if(isset($related_btn['edit']) && in_array($tbl,$related_btn['edit'])) $delete_flag = 1;

        if(isset($related_btn['new']) && in_array($tbl,$related_btn['new'])){
            $btn_new = "<br /><button class=\"btn btn-warning new\" type=\"button\" data-editable=\"$edit_flag\" data-deletable=\"$delete_flag\" data-table=\"$tbl\" name=\"newbtn\" id=\"newbtn\" value=\"new\" data-type=\"new\" data-toggle=\"modal\" data-target=\"#childModal\"/>New</button>";
        }
        if($tbl == 'assessment')
            $tb=$tb."<div role=\"tabpanel\" class=\"tab-pane active\" id=\"$tbl\">";
        else
            $tb=$tb."<div role=\"tabpanel\" class=\"tab-pane\" id=\"$tbl\">";
        $k++;
        if($tbl!="activitylog")
            $tb=$tb.$btn_new;
        // --------
        $tg_top="<div class=\"table-responsive\"><table class=\"table table-hover\" width=auto >";
                $tg_hdr="<th>";
                $tg_hdr_cl="</th>";
                //$tg_ro="<tr class=\"clickableRow\" href=\"".$link."constant\">";
                $tg_ro="<tr>";
                $tg_ro_cl="</tr>";
                $tg_td="<td>";
                $tg_td_cl="</td>";
            $tg_top_cl="</table></div>";

        //$tg_top="<div><ul class=\"nav nav-tabs nav-stacked\" id=\"".$tbl."\" >";
        $tg_hdr="<th>";
        $tg_hdr_cl="</th>";
        $tg_al="<a href=\"#\" onclick=\"openPage(";
        $tg_alo=");\" >";
        $tg_alc="</a>";
        $tg_ip="<input";
        $tg_ip_type=" type=\"";
        $tg_ip_name="\" name=\"";
        $tg_ip_value="\" value=\"";
        $tg_ip_size="\" size=\"";
        $tg_attr_id=" data-id=\"";
        $tg_attr_tbl="\" data-tbl=\"";
        $tg_span="<span ";
        $tg_span_cl="</span>";
        $tg_ip_id="\" id=\"";
        $tg_class="\" class=\"ro";
        $tg_ip_cl="\" />";
        $tg_chk="<input type=\"checkbox\" name=\"chb";
        $tg_chk_val="";
        $tg_dat="<a onclick=\"customeDateTimepicker('";
        $tg_dat_cl="','yyyymmdd')\"><img src=datetimepick/cal.gif width=16 height=16 border=0 alt=Pick a date></a>";
        $tg_sel="<select name=\"";
        $tg_cl="\" >";
        $tg_sel_cl="</select>";
        $tg_opt="<option value=\"";
        $tg_opt_cl="</option>";
        $tg_hidden="hidden";
        $tg_readonly="\" readonly ";
        $tg_text="<textarea rows=\"4\" cols=\"15\" ";
        $tg_text_cl="</textarea>";
        // $show=array('id','tbl','qual','action','runon','fld','oper','val','actionid','notification','datequantifiers');

        $a="";
    //Open Table
    $q="select showfields from workflow where formname='$tbl'";
    $res=$db_model->first($q);
    $show=array();
            if($res)
            {
                $fieldstring=$res->showfields;
                $show=explode(',',$fieldstring);
                array_unshift($show,"id");
            }
            else
            {
                // echo "in else";
                $columnnamequery=" select column_name from information_schema.columns where table_name='$tbl'";
                $res=$db_model->allArray($columnnamequery);
                // while($row=mysql_fetch_assoc($res))
                foreach($res as $row)
                {

                    array_push($show,$row['column_name']);
                }
            }
            $a= $tg_top;
    //Print Header row
            $a=$a."<tr>";
            foreach($show as $col)
            {
                $rs=$db_model->first("select alias from field where name='$col'");
                if($rs)
                {
                    $alias=$rs->alias;
                }
                else
                    $alias=$col;

            $a=$a.$tg_hdr.$alias.$tg_hdr_cl;
            }
            $a=$a.$tg_hdr."".$tg_hdr_cl;
            $a=$a.$tg_hdr."".$tg_hdr_cl;

            $a=$a."</tr>";
            $a=$a.$tg_ro;

            $q="select showfields from workflow where formname='$tbl'";
            $res=$db_model->first($q);
            if($res)
            {
                $fieldstring=$res->showfields;
                $ff=explode(",",$fieldstring);
                if(!in_array("id",$ff))
                {
                    array_unshift($ff,"id");
                }
                $fieldstring=implode(",",$ff);



            //     echo $fieldstring;
            //     // $data_sql="select".$fieldstring." from ".$tbl." where id in (select childrecid from reldata where relid=".$id." and  parentrecid=".$pid.")";
            // }
            // if($tbl=="comments")
                $qual="1=1";
                $data_sql="select $fieldstring from ".$tbl." where $qual and  id in (select childrecid from reldata where relid=".$id." and  parentrecid=".$pid.") order by id desc";
            // else
            // $data_sql="select * from $tbl where id in (select childrecid from reldata where relid=".$id." and  parentrecid=".$pid.")";
            }
            else
            {
                $data_sql="select * from $tbl where id in (select childrecid from reldata where relid=".$id." and  parentrecid=".$pid.") order by id desc";
            }
            // echo $data_sql;
            $res=$db_model->allArray($data_sql);
            if($res)
            {
                // while($row=mysql_fetch_object($res))
                foreach($res as $row)
                {
                    $a=$a.$tg_ro;

                        foreach($row as $field => $value)
                        {
                            // print_r($row);
                            if($field=="id")
                                $x=$value;
                            $field_name=$field;
                            $rs=$db_model->first("select tblid from config where name='$tbl'");
                            $childtblid=$rs->tblid;
                            $rss=$db_model->first("select * from field where name='$field_name' and tblid='$childtblid'") or die("error7".mysql_error());
                            if($rss)
                            $fieldtype=$rss->type;
                            $field_index=$rss->dbindex;
                            $fieldid = $rss->fieldid;
                            $i=0;
                            if($field_index == 'primary'){
                                $a=$a.$tg_td.$tg_span.$tg_attr_id.$value.$tg_attr_tbl.$tbl.$tg_cl.$value.$tg_span_cl.$tg_td_cl;
                            }
                            elseif($fieldtype=="file" )//////changes
                            {
                                // echo $fieldtype;
                                    $a=str_replace('constant', $value,$a);

                                //genrate path of a file////////
                                $qry_for_f_path="select upload_dir from admin where table_name='".$tbl."'";
                                $path=$db_model->first($qry_for_f_path);
                                if($path)
                                {
                                    $file_path=$path->upload_dir;
                                    $qry_for_f_name="select ".$field." from ".$tbl." where ".$field."='".$value."'";
                                    $name=$db_model->first($qry_for_f_name);

                                    if($name)
                                    {
                                        $file_name=$name->$field;
                                        $file_names=explode(',',$file_name);
                                        $a=$a.$tg_td;
                                        foreach($file_names as $key => $file_name)
                                        {

                                            $a=$a."<a href='".$file_name."' target=\"_blank\">".$file_name."</a><br>";
                                        }
                                    }else{

                                        $a=$a.$tg_td.$tg_td_cl;
                                    }
                                }else{

                                    $a=$a.$tg_td."<a href='".$value."' target=\"_blank\">".$value."</a>".$tg_td_cl;
                                }
                            } ////changes
                       elseif($fieldtype=="list"){
                            if($value){
                                $a=$a.$tg_td.$utils->getFirstListAlias($fieldid,$value).$tg_td_cl;
                            }else{
                                $a=$a.$tg_td.$value.$tg_td_cl;
                            }
                       }
                       elseif($fieldtype=="option"){
                            if($value){
                                $a=$a.$tg_td.$utils->getOptionAlias($fieldid,$value).$tg_td_cl;
                            }else{
                                $a=$a.$tg_td.$value.$tg_td_cl;
                            }
                       }
                       elseif($fieldtype=="idate"){
                           if($value){
                               $a=$a.$tg_td.$utils->getmydate($value).$tg_td_cl;
                            }else{
                                $a=$a.$tg_td.$value.$tg_td_cl;
                           }

                       }else{

                           $a=$a.$tg_td.$value.$tg_td_cl;
                       }

                        }
                        // if($tbl!="activitylog")
                        if(isset($related_btn['edit']) && in_array($tbl,$related_btn['edit'])){
                            $a=$a.$tg_td."<br /><button class=\"btn btn-success editbtn\" type=\"button\" name=\"edit[$x]\" value=\"edit\" data-table=\"$tbl\"  data-ptbl=\"$parenttbl\"  data-type=\"edit\" data-id=\"$x\" data-toggle=\"modal\" data-target=\"#childModal\">Edit</button>".$tg_td_cl;
                        }
                        if(isset($related_btn['delete']) && in_array($tbl,$related_btn['delete'])){
                            $a .=$tg_td."<br /><button class=\"btn btn-danger deletebtn\" type=\"button\" name=\"del[$x]\" value=\"delete\" data-table=\"$tbl\"  data-ptbl=\"$parenttbl\"  data-type=\"delete\" data-id=\"$x\" >Delete</button>".$tg_td_cl.$tg_ro_cl;
                        }
                    $a=$a.$tg_ro_cl;

                //close while loop
                }
            }

        $a=$a.$tg_top_cl;
        //    echo $a;


        // ----------

        $tb=$tb.$a."</div>";
    }
    $tb=$tb."</div>";
    $tb=$tb."</div>";
    return $tb;
}