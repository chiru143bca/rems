<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="customer-related-modal" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"> <b>Confirmed orders waiting to be dispatched</b> </h4>
        </div>
        <div class="modal-body">
          <p></p>
        </div>
        <div class="modal-footer"  id="alert-alert">
          <div class="col-md-2 pull-right"><button type="button" class="btn btn-primary" data-dismiss="modal">OK</button></div>
        </div>
        <!-- <div class="modal-footer" id="alert-confirm">
          <button type="button" class="btn btn-default" id="alert-confirm-false" >Cancel</button>
          <button type="button" class="btn btn-primary" id="alert-confirm-true" >Ok</button>
        </div> -->
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->