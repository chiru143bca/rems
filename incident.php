<?php
$_GET['page']='text.php';
require_once("auth.php");
if($_POST){
    if(isset($_POST['submit']) || isset($_POST['update'])){
        $s_description = $_POST['short_description'];
        $subcategory = $_POST['subcategory'];
        $state = $_POST['state'];
        $json = array('short_description'=>$s_description,'subcategory'=>$subcategory,'state'=>$state);
        if(isset($_POST['submit'])){
            DataMapulation(json_encode($json),'POST','');
        }else{
            $sysid = $_POST['sys_id'];
           $res = DataMapulation(json_encode($json),'PUT','/'.$sysid);
        }
    }
}


$edit = (object) array('short_description'=>'','state'=>'','sys_id'=>'','subcategory'=>'','number'=>'');
if(isset($_GET['number'])){
    $number = $_GET['number'];
    $edit = DataMapulation('[]','GET','?number='.$number);
    $edit = $edit->result[0];
}

$datas = DataMapulation('[]','GET','');
$datas = $datas->result;
function DataMapulation($json="[]",$type="GET",$param=""){
    // $json = addslashes($json);
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://dev72861.service-now.com/api/now/table/incident".$param,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => $type,
    CURLOPT_POSTFIELDS => $json,
    CURLOPT_HTTPHEADER => array(
        "authorization: Basic YWRtaW46SW5zdGFuY2Ux",
        "cache-control: no-cache",
        "content-type: application/json",
    ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
    echo "cURL Error #:" . $err;
    } else {
    return json_decode($response);
    }
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Incident</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<div class="container">
<div class="clearfix">&nbsp;</div>
<legend>Incident <?php if(isset($_GET['number'])){ ?><a href="incident.php" class="btn btn-success">New</a><?php } ?></legend>
    <div class="col-md-12">
        <form action="" method="post" class="form-horizontal">
            <div class="form-group">
                <label for="">Short Description</label>
                <input type="text" class="form-control" name="short_description" value="<?php echo $edit->short_description; ?>">
            </div>
            <div class="form-group">
                <label for="">Subcategory</label>
                <input type="text" class="form-control" name="subcategory" value="<?php echo $edit->subcategory; ?>">
            </div>
            <div class="form-group">
                <label for="">State</label>
                <?php $states= array("select State","New","In Progress","On Hold"); ?>
                <select name="state" id="state">
                <?php foreach($states as $key=>$val){ ?>
                    <option value="<?php echo $key ?>" <?php if($key == $edit->state) { echo 'selected';} ?>><?php echo $val; ?></option>
                <?php } ?>
                </select>
            </div>
            <div class="form-group">
            <?php if(isset($_GET['number'])) {?>
            <input type="hidden" name="sys_id" value="<?php echo $edit->sys_id ?>">
            <button type="submit" class="btn btn-info" name="update">Update</button>
            <?php }else{ ?>
            <button type="submit" class="btn btn-info" name="submit">Submit</button>
            <?php } ?>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Incident Number</th>
                    <th>Short Description</th>
                    <th>Sub Category</th>
                    <th>State</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($datas as $data){ ?>
                    <tr>
                        <td><?php echo $data->number; ?></td>
                        <td><?php echo $data->short_description; ?></td>
                        <td><?php echo $data->subcategory; ?></td>
                        <td><?php echo isset($states[$data->state]) ? $states[$data->state] :''; ?></td>
                        <td><a href="?number=<?php echo $data->number; ?>">Edit</td>
                    </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>