<?php
session_start();
require_once('lib/CoreView.php');
require_once('lib/Utils.php');
require('./fpdf/html_table.php');
$coreV = new CoreView();
$utils = new Utils();
// $sql1=$coreV->dbsql();
$user_id = $_SESSION['SESS_id'];
$content ="";
if(isset($_POST['offer_content']) && $_POST['offer_content']){
    $id = $_POST['id'];
    $ctc = $_POST['ctc'];
    $content = $_POST['offer_content'];
    $content = $utils->contentMap($content,array('candidates'=>$id,'candidate_det'=>$ctc));
    $content =$content['content'];
}
$candidate = $utils->first("select * from candidates where id = '{$id}'");
$details = $utils->first("select * from candidate_det where id = '{$ctc}'");
$name = str_replace(' ','_',$candidate->candidate_name);
$relrecid = $utils->first("select id from formrel where parent = 'candidates' and child = 'attachments'");
$relrecid = $relrecid->id;
$tot_attachment = $utils->first("select count(*) as aggregate from reldata where relid = '{$relrecid}' and parentrecid ='$id'");
$revision = $tot_attachment->aggregate ? $tot_attachment->aggregate+1 : 299;
$sequence = $utils->getOfferSequence();
$ref_no = 'Ref No. IZT/HR/APL/'.$sequence;
$content = $utils->parseFilter($content,array('REF_NUM'=>$ref_no));
$sequence = 'Of_'.date('Y').'_'.$sequence.'_'.$revision;
$filename = $sequence.'.pdf';
$pdf=new PDF_HTML_Table();
$pdf->AddFont('Calibri','','calibri.php');
$pdf->SetFont('Calibri','',12);
$pdf->SetAuthor('Bhoopal T');
$pdf->AddPage();
$pdf->SetTopMargin(25);
$pdf->Header();
// $pdf->SetFont('Arial','',12);
$pdf->WriteHTML($content);
// $pdf->Footer();
$pdf->Output('public/attachments/'.$filename);
$path = 'public/attachments/'.$filename;
$time = time();
$date = $utils->getmydate($time);
$insert = "insert into attachments (created_at,created_by,status,attachment,attch_date) values
            ('$time','$user_id','1','{$path}','{$time}')";
$last_id = $utils->insertData($insert,true);
$insert_reldata = "insert into reldata (relid,parentrecid,childrecid) values({$relrecid},{$id},{$last_id})";
$utils->executeQuery($insert_reldata);
$tr = "<tr><td>$last_id</td><td><a href=\"{$path}\">{$path}</a></td><td>{$date}</td></tr>";

echo json_encode(array("success"=>$tr));


exit;