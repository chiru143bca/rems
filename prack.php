
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>InputZero Technologies Pvt. Ltd</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Tell the browser to be responsive to screen width -->
  <!-- <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> -->
  <link rel="stylesheet" type="text/css" href="css/templateblue.css" />
	<!-- <link href="css/bootstrap.min.css" rel="stylesheet"/> -->
	<!-- <link href="css/style.css" rel="stylesheet"/> -->
	<link href="css/sbar.css?d=<?php echo time(); ?>" rel="stylesheet"/>
	<link href="css/toggleswitch.css" rel="stylesheet"/>
	<link href="plugin/jquery-ui-1.11.4/jquery-ui.css" rel="stylesheet"/>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css"> -->
  <!-- jvectormap -->
  <!-- <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <!-- <link rel="stylesheet"  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
  <!-- Fav and touch icons -->

  <style>
  @print{
    @page :footer {color: #fff }
    @page :header {color: #fff}
}
@page { size: auto;  margin: 12mm; }
@media print {
  body {
    margin: 0;
    color: #000;
    background-color: #fff;
  }
  title{
    color:red;
  }
}
  </style>
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="col-xs-6 form-group">
                    <label>Label1</label>
                    <input class="form-control" type="text"/>
                </div>
                <div class="col-xs-6 form-group">
                    <label>Label2</label>
                    <input class="form-control" type="text"/>
                </div>
                <div class="col-xs-6 form-group">
                    <label>Label1</label>
                    <input class="form-control" type="text"/>
                </div>
                <div class="col-xs-6 form-group">
                    <label>Label2</label>
                    <input class="form-control" type="text"/>
                </div>
                <div class="col-xs-6 form-group">
                    <label>Label1</label>
                    <input class="form-control" type="text"/>
                </div>
                <div class="col-xs-6 form-group">
                    <label>Label2</label>
                    <input class="form-control" type="text"/>
                </div>
                <div class="col-xs-6 form-group">
                    <label>Label1</label>
                    <input class="form-control" type="text"/>
                </div>
                <div class="col-xs-6 form-group">
                    <label>Label2</label>
                    <input class="form-control" type="text"/>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
