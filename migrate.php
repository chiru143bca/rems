<?php
   require_once('lib/DbModel.php');
   $db_model = new DbModel();

$alltables=array();
$tbl_sql="select name,alias from config";
$result=mysql_query($tbl_sql) or die("config".mysql_error());
while($row=mysql_fetch_assoc($result))
{
    $alltables[$row['name']]=$row['alias'];
    // array_push($tables,$row['name']);
}
$admin_tabs=["config","field","field_option","access","groups","Users","Events","valuelist"];
$tables=array();
$tables=array_diff($alltables,$admin_tabs);



?>
<br>
<h2 align="center">Export Migration Table</h2>

<?php if(isset($_SESSION['error'])){ ?>
<div class="alert alert-danger">
    <?php echo $_SESSION['error']; ?>
</div>
<?php unset($_SESSION['error']);  } ?>

    <form role="form" align="center">
        <div class="col-md-4">
            <div class="form-group ">
                <label for="table_name">Export Table:</label>
                <select class="form-control" name="table_name">
                    <?php
                        foreach($tables as $key => $value)
                        echo "<option value=".$key.">".$value."</option>";
                    ?>
                </select>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="chk" id="chk" onclick="tab_change()">change name
                </label>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="form-group">
                <label for="file_name">Export File:</label>
                <input type="text" class="form-control" name="file_name" id="file_name" placeholder="Enter File Name"></input>
            </div>
                <button id="submit" type="submit" class="btn btn-success">Export Migration</button>
            </div>
            <div class="col-md-1">&nbsp;</div>
        <div class="col-md-4 new-name" style="display:none;border:1px solid #cce;padding:30px;">
            <div class="form-group ">
                <label for="new_tab" id="new_tab_label">Destination Table Name</label>
                <input type="text" class="form-control" name="new_tab" id="new_tab" placeholder="Enter New Name"></input>
            </div>
            <div class="form-group ">
                <label for="new_alias" id="new_alias_label">Destination Table Alias Name</label>
                <input type="text" class="form-control" name="new_alias" id="new_alias" placeholder="Enter New Alias Name"></input>
            </div>
            </div>
    </form>
<script type="text/javascript">
    var form = document.getElementById('frm1').setAttribute('action', 'export-migration.php');
    
    function tab_change() {
        var checkbox = document.getElementById('chk');
        if (checkbox.checked == true) {
            $('.new-name').show();
        } else {
            $('.new-name').hide();
        }
    }
</script>