<?php
session_start();
require_once('lib/CoreView.php');
require_once('lib/Utils.php');
$coreV = new CoreView();
$utils = new Utils();
// $sql1=$coreV->dbsql();
$user_id = $_SESSION['SESS_id'];
if(isset($_POST['email']) && $_POST['email']){
    $id = $_POST['id'];
    $email = $_POST['email'];
    $cc = isset($_POST['cc'])? $_POST['cc']:NULL;
    $msg = $_POST['msg'];
    $to = explode(',',$email);
    $res = isset($_POST['attachments'])?$_POST['attachments']:"";
    $subject = $_POST['subject'];
    // update sent time
    $time = time();
    foreach($to as $toaddress){
        $content = $utils->contentMap($msg,array('candidates'=>$id));
        $msg = $content['content'];
        $msg = addslashes($msg);
        $subject_content = $utils->contentMap($subject,array('candidates'=>$id));
        $subject = $subject_content['content'];
        $subject = addslashes($subject);
        $insert =  "insert into notification_log (created_at,created_by,template,recipient,recipient_email,attachment,send_status,send_remark,notification_subject,cc,tbl,row_id) values
        ({$time},{$user_id},'".$msg."','".$toaddress."','".$toaddress."','".$res."','initiated','NA','{$subject}','$cc','candidates','{$id}')";
        $last_id = $utils->insertData($insert,true);
        $relrecid = $utils->first("select id from formrel where parent = 'candidates' and child = 'notification_log'");
        $relrecid = $relrecid->id;
        $insert_reldata = "insert into reldata (relid,parentrecid,childrecid) values({$relrecid},{$id},{$last_id})";
        $utils->executeQuery($insert_reldata);
    }
    echo json_encode(array('success'=>'Success'));exit;
}elseif(isset($_POST['ctc'])){
    $id = $_POST['ctc'];
    $ctc_sql = "select * from candidate_det where id = {$id}";
    $data = $utils->firstArray($ctc_sql);
    if($data && !empty($data)){
        $tbl = "<table border='1'><tr><th>Salary</th><th>Annual</th><th>Monthly</th></tr>";
        $band = $data['band'];
        $template = $data['template'];
        $components = $utils->all("select * from attributes where band='{$band}' and template_name = '{$template}' order by component_sorting asc");
        foreach($components as $component){
            if(isset($data[$component->attribute])){
                $tbl .= "<tr><td>".$component->alias."</td>";
                $tbl .= $component->yearly ? "<td>".$data[$component->attribute]."</td>" :'<td>&nbsp;</td>';
                $tbl .= $component->monthly ?"<td>".round(($data[$component->attribute]/12),2)."</td></tr>":'<td>&nbsp;</td>';
            }
        }
        $tbl .= "</table>";
        $res = array('success'=>$tbl);
        echo json_encode($res);
        exit;

    }
}elseif(isset($_POST['offer_ctc'])){
    $id = $_POST['offer_ctc'];
    $offer_template = $_POST['offer_template'];
    $ctc_sql = "select * from candidate_det where id = {$id}";
    $offer_template_sql = "select * from letter_templates where id = '{$offer_template}' ";
    $offer_data = $utils->first($offer_template_sql);
    $data = $utils->firstArray($ctc_sql);
    $tbl = "";
    $content = "";
    if($data && !empty($data)){
        $tbl .= "<table border='1'><tr><td width=\"350\" align='left' bgcolor=\"#D0D0FF\"><strong>Salary</strong></td><td bgcolor=\"#D0D0FF\"><strong>Annual</strong></td><td bgcolor=\"#D0D0FF\"><strong>Monthly</strong></td></tr>";
        $band = $data['band'];
        $template = $data['template'];
        $components = $utils->all("select * from attributes where band='{$band}' and template_name = '{$template}' order by component_sorting asc");
        foreach($components as $component){
            if(isset($data[$component->attribute])){
                $tbl .= "<tr><td width=\"350\">".$component->alias."</td>";
                $tbl .= $component->yearly ? "<td>".round($data[$component->attribute])."</td>" :'<td>&nbsp;</td>';
                $tbl .= $component->monthly ?"<td>".round(($data[$component->attribute]/12))."</td></tr>":'<td>&nbsp;</td>';
            }
        }
        $tbl .= "</table>";
    }
    if($offer_data){
        $content .= $offer_data->content;
        $content .= $tbl;
    }
    $res = array('success'=>$content);
    echo json_encode($res);
    exit;
}elseif(isset($_POST['preview'])){
    $id = $_POST['id'];
    $msg = $_POST['preview'];
    $ctc = $_POST['ctc_preview'];
    // $ctc_word = $utils->inrWord($ctc['ctc']);
    // echo $ctc_word;
    $content = $utils->contentMap($msg,array('candidates'=>$id,'candidate_det'=>$ctc));
    $contents = $content['content'];

    $res = array('success'=>$contents);
    echo json_encode($res);
    exit;

}
