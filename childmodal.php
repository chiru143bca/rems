<div class="modal fade" id="childModal" tabindex="-1" role="dialog" aria-labelledby="childModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="childModalLabel" contenteditable="true">New</h4>
      </div>
      <div class="modal-body" >
      <?php $x=explode('=',$sel);
            $pid = "1";
            if(isset($x[1])) $pid= $x[1];
            ?>
            <div id="childviewdiv">
            </div>
            <div id="child">
            <input type="hidden" name="parenttbl" id="parenttbl" value="<?php echo $tbl; ?>">
            <input type="hidden" name="recordid" id="recordid">
            <input type="hidden" name="childtbl" id="childtbl">
            <input type="hidden" name="action" id="action">
            <input type="hidden" name="pid" id="pid" value="<?php echo $pid; ?>">
            </div>
       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="closebtn" data-dismiss="modal"  >Close</button>
        <button type="button" class="btn btn-primary dbform" id="insertbtn" data-type="insert"  >Add</button>
        <button type="button" id="updatebtn" class="btn btn-primary" data-type="update" >SAVE</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>