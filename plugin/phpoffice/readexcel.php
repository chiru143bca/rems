<?php

require_once 'excel/Classes/PHPExcel.php';
require_once('lib/DbModel.php');
require_once('lib/Utils.php');
$db_model = new DbModel();
$utils = new Utils();
   $inputFileName=isset($inputFileName)?$inputFileName:'samples/axisbank.xlsx';
   $inputTable=isset($inputTable)?$inputTable:'bank';
    $mfields=array();
$mfields=$utils->tbldef($inputTable);
// print_r($mfields);
//$inputFileType = PHPExcel_IOFactory::identify('samples/CourseDetails.xlsx');
$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);  

                $objReader->setReadDataOnly(true);

                /**  Load $inputFileName to a PHPExcel Object  **/  
                $objPHPExcel = $objReader->load($inputFileName);

                $total_sheets=$objPHPExcel->getSheetCount(); 

                $allSheetName=$objPHPExcel->getSheetNames(); 
                $objWorksheet = $objPHPExcel->setActiveSheetIndex(0); 
                $highestRow = $objWorksheet->getHighestRow(); 
                $highestColumn = $objWorksheet->getHighestColumn();  
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);  
                for ($row = 1; $row <= $highestRow;++$row) 
                {  
                    for ($col = 0; $col <$highestColumnIndex;++$col)
                    {  
                        $value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();  

                              $arraydata[$row-1][$col]=$value; 
								// echo $value.$col.$row;

                    }  

				}
$tg_top="<table width=auto border=1 cellpadding=2 cellspacing=2>";
	$tg_hdr="<th>";
	$tg_hdr_cl="</th>";
	$tg_ro="<tr>";
	$tg_ro_cl="</tr>";
	$tg_td="<td>";
	$tg_td_cl="</td>";
	$tg_top_cl="</table>";
	$tg_ip="<input";
	$tg_ip_type=" type=\"";
	$tg_ip_name="\" name=\"";
	$tg_ip_value="\" value=\"";
	$tg_ip_size="\" size=\"";
	$tg_ip_id="\" id=\"";
	$tg_ip_cl="\" />";
	$tg_sel="<select id=\"";
	$tg_cl="\" >";
	$tg_selected = "\" selected=\"";
	$tg_sel_cl="</select>";
	$tg_opt="<option value=\"";
	$tg_opt_cl="</option>";
	$hdr=array();
	$arr=array();
echo $tg_top;
$cnt=0;$a='';
for($i=0;$i<=sizeof($arraydata);$i++)
{
$l=1;
if(isset($arraydata[$i])){
echo $tg_ro;
	for($j=0;$j<sizeof($arraydata[$i]);$j++)
        {$a='';
		//echo $tg_td.$arraydata[$i][$j].$tg_td_cl;
		if($i==0 && isset($arraydata[$i][$j]))
		{$keys=$arraydata[$i][$j];
		if(array_key_exists($keys,$mfields))
		{
		$hdr[$j]=$arraydata[$i][$j];
		$arr[$j]=$mfields[$hdr[$j]];
		}
		}
		
		if(isset($hdr[$j]))
		{
		if($i==0)
		{echo $tg_td.$arraydata[$i][$j].$tg_td_cl;
		}elseif($mfields[$arr[$j]."type"]=="idate")
		{echo $tg_td.$tg_ip.$tg_ip_type."text".$tg_ip_name.$mfields[$hdr[$j]].($i-1).$tg_ip_size."10".$tg_ip_value.$utils->getmydate(($arraydata[$i][$j]-25569)*86400).$tg_ip_cl.$tg_td_cl;
		}elseif($mfields[$arr[$j]."type"]=="textarea")
		{echo $tg_td.$tg_ip.$tg_ip_type."text".$tg_ip_name.$mfields[$hdr[$j]].($i-1).$tg_ip_size."60".$tg_ip_value.addslashes($arraydata[$i][$j]).$tg_ip_cl.$tg_td_cl;
		}elseif($mfields[$arr[$j]."type"]=="option")
					{					$a=$a.$tg_td.$tg_sel.$mfields[$hdr[$j]].($i-1).$tg_ip_name.$mfields[$hdr[$j]].($i-1).$tg_cl;
										//$a=$a.$tg_opt.$datarow[$row['name']].$tg_cl.$datarow[$row['name']].$tg_opt_cl;
										//if(($i-1)==0)
										//{
											$sql_opt="select * from field_option where tblid=".$mfields[$arr[$j]."tblid"]." and fieldid=".$mfields[$arr[$j]."fieldid"];
											$result_opt=$db_model->allArray($sql_opt);
											$opt[$l]="";
											// while($row_opt = mysql_fetch_array($result_opt))
											foreach($result_opt as $row_opt)
												{
													$selected = trim($row_opt['alias']) == trim($arraydata[$i][$j]) ? "\" selected data-test=\"" : "";
												$opt[$l]=$opt[$l].$tg_opt.$row_opt['value'].$selected.$tg_cl.$row_opt['alias'].$tg_opt_cl;
												if($row_opt['value']==$arraydata[$i][$j]) 
													{$alias=$row_opt['alias'];}
												}
										//}
										//below handles code if no value is set and also instead of value shows alias
										$alias=(isset($alias)&&($alias!=null))?$alias:"--NONE--";
										//echo "alias is".$alias;
										// $a=$a.$tg_opt.$arraydata[$i][$j].$tg_cl.$arraydata[$i][$j].$tg_opt_cl;
										//introducing for clear value option
										$a=$a.$tg_opt."".$tg_cl."--NONE--".$tg_opt_cl;
										$a=$a.$opt[$l].$tg_sel_cl.$tg_td_cl;
										echo $a;
					$l++;
					}elseif($mfields[$arr[$j]."type"]=="list")
					{
								$a=$a.$tg_td.$tg_sel.$mfields[$hdr[$j]].($i-1).$tg_ip_name.$mfields[$hdr[$j]].($i-1).$tg_cl;
											//adding new on13/12/2014.can be deleted
											$fid=$mfields[$arr[$j]."fieldid"];
											$tid=$mfields[$arr[$j]."tblid"];
											$sql_filter="select source,filter,id,value,alias from valuelist where id in (select optid from field_option where tblid=".$tid." and fieldid=".$fid.")";
											//echo $sql_filter;
											$result_filter=$db_model->first($sql_filter);
											$vsource=$db_model->source;
											$vfilter=$db_model->filer;
											$vid=$db_model->id;
											$vvalue=$db_model->value;
											$valias=$db_model->alias;
											$vfilter= $vfilter==null?" where 1=2 ":" where ".$utils->parseFilter($vfilter);
											$sql_opt="select ".$vvalue." 'value',".$valias." 'alias' from ".$vsource.$vfilter;
											//echo $sql_opt;	
											//Completion of change
											$result_opt=$db_model->allArray($sql_opt);
											$opt[$l]="";
											$alias='';
											if($result_opt)
											{
											// while($row_opt = mysql_fetch_array($result_opt))
												foreach($result_opt as $row_opt)
												{
												$opt[$l]=$opt[$l].$tg_opt.$row_opt['value'].$tg_cl.$row_opt['alias'].$tg_opt_cl;
												if($row_opt['value']==$arraydata[$i][$j]) 
													{$alias=$row_opt['alias'];}
												}
											}
										//}
										//below handles code if no value is set and also instead of value shows alias
										$alias=(isset($alias)&&($alias!=null))?$alias:"--NONE--";
										//echo "alias is".$alias;
										$a=$a.$tg_opt.$arraydata[$i][$j].$tg_cl.$arraydata[$i][$j].$tg_opt_cl;
										//introducing for clear value option
										$a=$a.$tg_opt."".$tg_cl."--NONE--".$tg_opt_cl;
										$a=$a.$opt[$l].$tg_sel_cl.$tg_td_cl;
										echo $a;
					$l++;
					}else
		echo $tg_td.$tg_ip.$tg_ip_type."text".$tg_ip_name.$mfields[$hdr[$j]].($i-1).$tg_ip_size."10".$tg_ip_value.str_replace(",","",$arraydata[$i][$j]).$tg_ip_cl.$tg_td_cl;
		}
		}
echo $tg_ro_cl;

		}
	}
echo $tg_top_cl;	
$cnt=sizeof($arraydata)-1;
		echo "<input type=\"number\" name=\"icnt\" value=$cnt style=\"display:none\">";
		echo "<input type=\"text\" name=\"field_edit\" value=\"".implode(",",$arr)."\" >";	
		//print_r($arraydata);

?>