
function cb(){}
var edit_record = false;
window.addEventListener('scroll',function() {
    //When scroll change, you save it on localStorage.
    localStorage.setItem('scrollPosition',window.scrollY);
},false);
window.addEventListener('load',function() {
    if(localStorage.getItem('scrollPosition') !== null)
       window.scrollTo(0, localStorage.getItem('scrollPosition'));
},false);
   
$('#childModal').on('hide.bs.modal', function (e) {
    tinymce.remove("#childviewdiv .ihtml");
    $('#childviewdiv').html('');
});
$('#childModal').on('show.bs.modal', function (e) {
    var  button= $(e.relatedTarget);
    var tbll=button.data('table');
    var act=button.data('type');
    
    if(act=="edit")
    {  
        edit_record = button.parent().parent();
        var id=button.data('id');
        $('#recordid').val(id);
        $('#updatebtn').show(); 
        $('#insertbtn').hide(); 
    }   
    if(act=="new")
    {
        var editable = button.data('editable');
        var deletable = button.data('deletable');
        $('#insertbtn').attr('data-editable',editable);
        $('#insertbtn').attr('data-deletable',deletable);
       $('#updatebtn').hide(); 
       $('#insertbtn').show(); 
    }   
    $('#childtbl').val(tbll);
});

$('.new').click(function(){

    var tbl=$(this).attr('data-table');
    $('#action').val("new");
    $.ajax({
        type: 'post',
        url : 'tablealias_ajax.php',
        data : {tbl:tbl},
        success : function(res){
            $('#childModalLabel').text(res);
        }
    });
    var act="new";  
    var dbr_request=$.ajax(
                {
                    method:"POST",
                    url:"relatedformsajax.php?page="+tbl,
                    // data:{comment:comment,childtbl:childtbl,action:action,pid:pid,parenttbl:parenttbl},
                    data:{tbl:tbl,act:act},
                    dataType:"html"
                })
                dbr_request.done(function(m){
                    $('#childviewdiv').html(m);
                    tinyMCE.init({
                        theme : "modern",
                        mode: "specific_textareas",
                        toolbar: "preview",
                        resize: 'both',
                        content_css : 'css/bootstrap.min.css',
                        editor_selector : "ihtml",
                        plugins : [ "code visualchars advlist table spellchecker textcolor searchreplace preview" ],
                        height:"auto",
                        width:"auto",
                        formats:{
                            underline: { inline: 'u', exact: true }
                        }
                    
                    });
                    if(tbl == 'quotations'){
                        $('#childviewdiv select[name=status0]').append("<option value='0' selected></option>").hide();
                        $('#childviewdiv select[name=status0]').parent().hide();
                    }
                    $('#btn').hide();
                     $('#insertbtn').prop('disabled', false);
                  
});
});
$('#insertbtn').click(function(e){
    var childtbl = $("#childviewdiv input[name=childtbl]").val();
   var editable = $(this).attr('data-editable');
   var deletable = $(this).attr('data-deletable');
    var description= $('input[name=description0]').val();
    var customer_name=$('input[name=name0]').val();
    var phone=$('input[name=phone0]').val();
    var email=$('input[name=email0]').val();
    var comment=$('input[name=comments0]').val();
    var reg = new RegExp('^\s*$|^[0-9]+$');
    var reg1 = new RegExp('^$|^[A-Za-z\\-\\s\\_]+$');
    var reg2=new RegExp('^\s*$|^[A-Za-z0-9\\-\\s\\_]+$');
    // var email_reg =new RegExp(' /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/');
    var phone_reg = new RegExp("^\s*$|^[0-9()\\-]+$");
    // if(description && !reg2.test(description.trim()))
    // {
    //     customAlert('Kindly Input Alphanumeric for Description',cb);
    //     return false;
    // }
     if(customer_name && !reg1.test(customer_name.trim()))
    {
        customAlert('Kindly Input alphabets for Name',cb);
        return false;
    }
    else if(comment && !reg2.test(comment.trim()))
    {
        customAlert("Kindly Input Alphanumeric for Comments",cb);
        return false;
    }
    // else if(email && !email_reg.test(email.trim()))
    // {
    //     customAlert('Kindly Check entered email address',cb);
    //     return false;
    // }
    else if(phone && !phone_reg.test(phone.trim()))
    {
        customAlert("Kindly use only {0-9, ( , ) , - } for phone number",cb);
        return false;
    }
    $('#action').val("insert");
    var tbl=$(this).attr('data-table');
    var form=$('#childModal input,#childModal select,#childModal textarea');
    var ihtmls = $('#childModal textarea.ihtml');
    var allformData = new FormData();
    for(var i=0; i<form.length;i++){
        allformData.append($(form[i]).attr('name'),$(form[i]).val());
    }
    if(ihtmls.length){
        $.each(ihtmls,function(k,v){
            var nme = $(v).attr('name');
            if(tinyMCE.get(nme)){
                var val = tinyMCE.get(nme).getContent();
                allformData.append(nme,val);
            }
        });
    }
    var childtblform=$('#childviewdiv input[type="text"]','#childviewdiv input[type="checkbox"]:checked','#childviewdiv input[type="radio"]:checked','#childviewdiv idate','#childviewdiv select','#childviewdiv textarea');
    for(var i=0; i<childtblform.length;i++){
        allformData.append($(childtblform[i]).attr('name'),$(childtblform[i]).val());
    }
    allformData.append('editable',editable);
    allformData.append('deletable',deletable);

    var childtblfiles=$('#childviewdiv input[type="file"]');
    for(var i=0; i<childtblfiles.length;i++){
        var filefield=$(childtblfiles[i]).attr('data-name');
        var fileInput = document.getElementsByName(filefield)[0];
        var file = fileInput.files;
        for(var j=0;j<file.length;j++)
        {
            allformData.append(filefield+'['+j+']',file[j]);
        }    
    }
    $('#insertbtn').prop('disabled', true);
    checkAutofill(allformData,function(re){
        if(re.error == 'no'){
            if(re.exchange){
                allformData.append("exchng_val0",re.exchange);
            }
            $.ajax({
                type:'post',
                url:"relatedformsajax.php?page="+tbl,
                contentType: false,
                processData: false,
                data:allformData,
                beforeSend: function(){
                    $('#insertbtn').prop('disabled', true);
                },
                complete : function(){
                    $('#insertbtn').prop('disabled', false);
                },
                success: function(data){
                    if(data){
                        data=data.trim();
                        if(data=="noquery")
                        {
                            $('#insertbtn').prop('disabled', false);
                        }
                        else
                        {
                            data = $.parseJSON(data);
                            // console.log(data);
                            var tbl=data.childtbl;
                            if($('#'+tbl+' '+'tbody').length){
                                $('#'+tbl+' '+'tbody tr:first').after(data.success);
                            }else{
                                $('#'+tbl).append("<tbody>"+data.success+"</tbody>");
                            }
                            $('#childModal').modal('hide');
                        }
                    }
                }
            })
        }else if(re.error == 'yes'){
            $('#insertbtn').prop('disabled', false);
            return false;
        }
    });    
});
$('#newcommentbtn').click(function(){
    var tbl=$(this).attr('data-table');
    var comment=$('#commentbox').val();
    var pid=$(this).attr('data-pid');
    var ptblid=$(this).attr('data-ptblid');
    var dbr_request=$.ajax(
                {
                    method:"POST",
                    url:"commentajaxnew.php?page="+tbl,
                    // data:{comment:comment,childtbl:childtbl,action:action,pid:pid,parenttbl:parenttbl},
                    data:{tbl:tbl,comment:comment,parentid:pid,parent_tbl_id:ptblid},
                    dataType:"html"
                })
                dbr_request.done(function(m){
                    // $('#collapseExample').collapse();
                    // $('#collapseExample').removeClass('collapse');
                    // $('#collapseExample').addClass('in');
                    // $('#activitiesbtn').removeClass('collapsed');
                    // $('#collapseExample').addClass('in');
                    // $('#collapseExample').attr('height',auto);
                    // console.log(m);
                    location.reload(true);
                });
});

$(function(){
    $('#childModal').on('click','.dellink',function(){
        var that = this;
        customConfirm("Do you want to remove the attachment? Clicking on yes will permanently delete it.",function(res){
            if(!res) return false;

            var cross = $(that);
            var common=$(that).attr('data-common');
            common='.'+common;
            var path=$(that).attr('data-path');
            var filename=$(that).attr('data-filename');
            var table=$(that).attr('data-table');
            var rowid=$(that).attr('data-rowid');
            var fieldname=$(that).attr('data-fieldname');
            $.ajax(
                {
                    method:"POST",
                    url:"deleteattachment.php",
                    data:{path:path,filename:filename,table:table,rowid:rowid,fieldname:fieldname},
                    dataType:"html",
                    success: function(res){
                        // console.log(res);
                        cross.remove();
                        $(common).remove();
                        $("#realtedForm a[href='"+filename+"']").remove();
                    }
                });
            });
        });
})
$('.dellink').click(function(){
    var cross = $(this);
    var common=$(this).attr('data-common');
    common='.'+common;
    var path=$(this).attr('data-path');
    var filename=$(this).attr('data-filename');
    var table=$(this).attr('data-table');
    var rowid=$(this).attr('data-rowid');
    var fieldname=$(this).attr('data-fieldname');
    $.ajax(
        {
            method:"POST",
            url:"deleteattachment.php",
            data:{path:path,filename:filename,table:table,rowid:rowid,fieldname:fieldname},
            dataType:"html",
            success: function(res){
                // console.log(res);
                 cross.remove();
                 $(common).remove();
            }
        });
});
$('#updatebtn').click(function(){

    $('#action').val("update");
    var that=$(this);
    var tbl = $("input[name=related_tbl]").val();
    var form=$('#childModal input,#childModal select,#childModal textarea');
    var ihtmls = $('#childModal textarea.ihtml');
    var allformData = new FormData();
    for(var i=0; i<form.length;i++){
        allformData.append($(form[i]).attr('name'),$(form[i]).val());
    }
    var childtblform=$('#childviewdiv input[type="text"]','#childviewdiv input[type="checkbox"]:checked','#childviewdiv input[type="radio"]:checked','#childviewdiv idate','#childviewdiv select','#childviewdiv textarea');
    for(var i=0; i<childtblform.length;i++){
        allformData.append($(childtblform[i]).attr('name'),$(childtblform[i]).val());
    }
    if(ihtmls.length){
        $.each(ihtmls,function(k,v){
            var nme = $(v).attr('name');
            if(tinyMCE.get(nme)){
                var val = tinyMCE.get(nme).getContent();
                allformData.append(nme,val);
            }
        });
    }

    var childtblfiles=$('#childviewdiv input[type="file"]');
    for(var i=0; i<childtblfiles.length;i++){
        var filefield=$(childtblfiles[i]).attr('data-name');
        var fileInput = document.getElementsByName(filefield)[0];
        var file = fileInput.files;
        for(var j=0;j<file.length;j++)
        {
            allformData.append(filefield+'['+j+']',file[j]);
        }    
    }     
   
//   console.log(allformData);return false;         
   $.ajax({
        type:'post',
        url:"relatedformsajax.php?page="+tbl,
        cache: false,
        contentType: false,
        processData: false,
        data:allformData,
        beforeSend: function(){
            $(that).prop('disabled',true);
        },
        complete: function(){
            $(that).prop('disabled',false);
        },  
        success: function(data){
            // console.log(ed);
            if(data){
                var data = $.parseJSON(data);
                edit_record.replaceWith(data.success);
            }
        }
    }) ; 
    $('#childModal').modal('hide');             
});

$('#realtedForm').on('click','.deletebtn',function(){
    var ptbl=$(this).attr('data-ptbl');
    var delid=$(this).attr('data-id');
    var childtbl=$(this).attr('data-table');
    var action=$(this).attr('data-type');
    var par = $(this).parent().parent();
    customConfirm("Are you sure you want to delete",function(result){
        if(!result) return false;
        else
        {
            var del_request=$.ajax(
                {
                    method:"POST",
                    url:"relatedformsajax.php?page="+ptbl,
                    data:{delid:delid,childtbl:childtbl,ptbl:ptbl},
                    dataType:"html"
                })
                del_request.done(function(delmsg){
                // console.log(delmsg);
                //   location.reload(true);
                    par.remove();
                });
        }
    });
   
});
$('.tab-pane').on('click','.editbtn',function(){
    var tbl=$(this).attr('data-table');
     $.ajax({
        type: 'post',
        url : 'tablealias_ajax.php',
        data : {tbl:tbl},
        success : function(res){
            $('#childModalLabel').text(res);
        }
    });
    var editid=$(this).attr('data-id');
    var childtbl=$(this).attr('data-table');
    var act="edit"; 
    var action=$(this).attr('data-type');
    $('#action').val("edit");
    var form=$('#childModal input,#childModal select,#childModal textarea');
    var data = {};
    for(var i=0; i<form.length;i++){
        data[$(form[i]).attr('name')] = $(form[i]).val();
    }
    var dbr_request=$.ajax(
                {
                    method:"POST",
                    // url:"relatedformsajax.php",
                    url:"relatedformsajax.php?page="+tbl,
                    // data:{comment:comment,childtbl:childtbl,action:action,pid:pid,parenttbl:parenttbl},
                    data:{tbl:childtbl,editid:editid,act:act},
                    dataType:"html"
                })
                dbr_request.done(function(m){
                    $('#childviewdiv').html(m);
                    console.log('ttd');
                    tinyMCE.init({
                        theme : "modern",
                        mode: "specific_textareas",
                        toolbar: "preview",
                        resize: 'both',
                        content_css : 'css/bootstrap.min.css',
                        editor_selector : "ihtml",
                        plugins : [ "code visualchars advlist table spellchecker textcolor searchreplace preview" ],
                        height:"auto",
                        width:"auto",
                        formats:{
                            underline: { inline: 'u', exact: true }
                        }
                    
                    });
                    $('#btn').hide();
                });
    // var request= $.ajax(
    //                     {
    //                         method:"POST",
    //                         url:"relatedformsajax.php?page="+tbl,
    //                         data:{editid:editid,childtbl:childtbl},
    //                         dataType:"json"
    //                     })
    //                     request.done(function(data){
    //                          console.log(data);
    //                         jQuery.each(data,function(index,item){
    //                             var ind=index;
    //                             var inde=index+"0";
    //                             var x=".inp[name='"+inde+"']";
    //                             console.log($(x));
    //                             // $(x).val("how");
    //                             $(x).val(item);
    //                         //   console.log($(x));  
    //                         //   console.log(  $("#childviewdiv .inp")); 
    //                             // var ele=document.getElementsByName(inde)[0].tagName;
    //                             // console.log(ele);
    //                             // z=ele+"[name='"+inde+"']";
    //                             // console.log(z);                                    
    //                             //  $(".inp[name='"+inde+"'].val(data."+ind+")");
    //                             // console.log($(".inp[name="+inde+"]").val("data."+ind));    
    //                             //    $(".inp[name='task_description0']").val(data.task_description);
    //                             //    $(".inp[name='index']").val(data.task_description);
    //                         });            
    //                     });    
});

function checkAutofill(data,cb){
    var childtbl = data.get('childtbl');
    var tax_type = data.get('tax_type0');
    var billing_currency = data.get('billing_currency0');
    var result = {error:"no"};
    if(childtbl == 'quotations'){
        $.ajax({
            type : 'post',
            data : {invoice_id: "sample",tax_type:tax_type,crncy_code:billing_currency},
            url : 'quotation-auto-fillform.php',
            success : function(res){
                var res = $.parseJSON(res);
                if(res.success){
                    result = {error:"no",exchange:res.exchange};
                }else if(res.error){
                    customAlert(res.error,cb);
                    result = {error:"yes"};
                }
                cb(result);
            }
        });

    }else {
        cb(result);
    }
    
}
 