var is_date_close=is_date_open="'";
var options = $("select[name=operator]").html();
$('#clear').click(function(){
    $('#qual_field').val("");
    $('#qual_hidden').val("");
    // $('#frm1').submit();

});
function today(){
	return 'today';
}
if(document.getElementsByName("fields")[0]){
	document.getElementsByName("fields")[0].onchange = function() {
		var field_ele = document.getElementsByName("fields")[0];
		var t = document.getElementsByName("qual_field")[0].value;
		var field = field_ele.value;
		var type = $(field_ele).find("option:selected").attr('data-type');
		var fieldid = $(field_ele).find("option:selected").attr('data-id');
		$('select[name=c_data]').hide().prop('disabled',true);
		$("select[name=operator]").attr('disabled',false).show();
		$("select[name=c_operend]").attr('disabled',true).hide();

		toggleInput(type,fieldid,function(){

			if(type == 'idate'){ 
				// $("input[name=data]").datepicker({dateFormat:'dd-mmm-yy'}); is_date_open='{{';is_date_close='}}';
				$('.relative-div').show();
				$("input[name=dateType]").prop('checked',false);
				var flag = true;
				if($('select[name=operator]').val() == 'between') flag = false;
				setDateRange(flag);
				// $("select[name=operator]").html("<option value='between'> between </option>");
				is_date_close="";is_date_open=""
				// is_date_open='{{';is_date_close='}}';
				$("input[name=dateType]").prop('checked',true);
				$("input[name=dateType]").trigger('change');
			}
			else{ 
				$('.relative-div').hide();
				$("select[name=operator]").html(options);
				$("input[name=data]").datepicker("destroy"); is_date_close="'";is_date_open="'"
			}
			if(field == 'fromdate' || field == 'todate' || field == 'doj'){
				document.getElementById("demo").innerHTML = 'Date Format Should be MM-DD-YYYY';
			}
			else{document.getElementById("demo").innerHTML = '';}
			// document.getElementsByName("qual_field")[0].value = t + " "+field;
			// document.getElementsByName("qual_hidden")[0].value = t + " "+field;
		});
	}
	
	document.getElementsByName("operator")[0].onchange = function() {
		var t = document.getElementsByName("qual_field")[0].value;
		var field = document.getElementsByName("operator")[0].value;
		var type = $("select[name=fields]").find("option:selected").attr('data-type');
		if($(this).val() == 'between' && type == 'idate'){
			setDateRange(false);
		}else if($(this).val() != 'between' && type == 'idate'){
			setDateRange(true);
		}
		// document.getElementsByName("qual_field")[0].value = t + ' '+field+' ';
		// document.getElementsByName("qual_hidden")[0].value = t + " "+field;
	}
	
	document.getElementsByName("data")[0].onchange = function() {
			var data = $("input[name=data]").val();
			var t = document.getElementsByName("qual_field")[0].value;
		
	}
	
	
	}
	
$(function(){
	$('select[name=c_fields]').on('change',function(){
		var type = $(this).find("option:selected").attr('data-type');
		if(type == 'idate'){ 
			$("input[name=c_data]").daterangepicker(
			{
			dateFormat:'dd-mm-yy', 
			singleDatePicker: true,
			showDropdowns: true,
			"locale": {
				"format": "YYYY-MM-DD"
			},
			ranges: {
				'Today': [moment()],
				'Tomorrow':[moment().add(1, 'days')],
				'Yesterday': [moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days')],
				'Last 30 Days': [moment().subtract(29, 'days')],
				'This Month': [moment().startOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month')]
		 	}
			}); 
			is_date_open = '{{';
			is_date_close = '}}';
			$("input[name=c_data]").show();
			$("input[name=c_data]").attr('disabled',false);
			$("select[name=c_data]").hide();
			$('.relative-div input').attr('checked',false);
			$("select[name=operator]").attr('disabled',false).show();
			$("select[name=c_operend]").attr('disabled',true).hide();
			$('.relative-div').show();
		}else{ 
			$("input[name=c_data]").show();
			$("select[name=c_data]").hide();
			$("input[name=c_data]").val('').data('daterangepicker').remove(); 
			$('.relative-div input').attr('checked',false)
			$('input[name=c_data]').attr('disabled',false);
			$("select[name=operator]").attr('disabled',false).show();
			$("select[name=c_operend]").attr('disabled',true).hide();
			$('.relative-div').hide();
			is_date_close="'";
			is_date_open="'";
		}
		
	});
	$('input[name=dateType]').change(function(){
		if($(this).is(':checked')){
			$("input[name=c_data]").attr('disabled',true).hide();
			$("select[name=c_data]").attr('disabled',false).show();
			$("input[name=data],select[name=operator]").attr('disabled',true).hide();
			$("select[name=data],select[name=c_operend]").attr('disabled',false).show();
		}else{
			$("input[name=c_data]").attr('disabled',false).show();
			$("select[name=c_data]").attr('disabled',true).hide();
			$("input[name=data],select[name=operator]").attr('disabled',false).show();
			$("select[name=data],select[name=c_operend]").attr('disabled',true).hide();
		}
	});
	// add btton
	$('#add_qual_main').click(function(){
		mainDataCondition('add');
	})
	$('#and').click(function(){
		var old_val = $('input[name=qual_field]').val();
		mainDataCondition('and');
	});
	$('#or').click(function(){
		var old_val = $('input[name=qual_field]').val();
		mainDataCondition('or');
	});
	// notification filter
	$('#c_and').click(function(){
		var old_val = $('input[name=qual_field]').val();
		dataCondition('and');
	});
	$('#c_or').click(function(){
		var old_val = $('input[name=conditions]').val();
		dataCondition('or');
	});
	$('#add_qual').click(function(){
		dataCondition('add');
	});
	$('#content-search').click(function(){
		var _input = $('input[name=content_search]');
		contentSearch(_input);
	});
	$('#search-log').on('show.bs.modal',function(e){
		var query = $("#qual_field").val();
		if(query.trim()){
			$('#search-query').val(query.trim());
			$('#search-text').text(query.trim());
		}
	});
	$('#search-log').on('hide.bs.modal',function(e){
		$('#search-query').val('');
		$('#search-name').val('');
		$('#search-text').text('');
	});
	$('#search-builder-submit').click(insertSearchBuilder);
	$('select[name=searched_query]').change(function(){
		var val = $(this).val();
		if(val){
			$('#qual_field').val(val);
		}
	});
	$('#run').click(function(e){
		e.preventDefault();
		// var href = new URL(window.location.href);
		var href = window.location.href+"&num=1";
		// console.log(href.source);
		mainDataCondition('add');
		// href.searchParams.set('num', '1');
		// var uri = href.toString();
		$('#frm1').attr('action',href);
		$('#frm1').submit();
	});
});

function dataCondition(con){
	var old_val = $('input[name=conditions]').val();
	var fields = $('select[name=c_fields]').val();
	var operator = $('select[name=c_operator]').val();
	var data = $('input[name=c_data]').val();
	var dateType = $('input[name=dateType]:checked').val();
	if(dateType) data =  $('select[name=c_data]').val();
	data = is_date_open+data+is_date_close;
	var mainval="";
	if(	fields && operator && data){
		var regex = /((\bor$\b)|(\band$\b))(?!.*\1)/gi;
		if(con == 'add'){
			if(old_val.trim() && regex.exec(old_val.trim()) === null) mainval = old_val.trim()+' and '+fields+' '+' '+operator+' '+data;
			else mainval = old_val.trim()+' '+fields+' '+' '+operator+' '+data;
		}
		else{
			// console.log(old_val.trim());
			if(!old_val.trim()){
				mainval = old_val.trim()+' '+fields+' '+' '+operator+' '+data+' '+con+' ';
			}else if(old_val.trim() && regex.exec(old_val.trim()) == null) {
				mainval = old_val.trim()+' '+fields+' '+' '+operator+' '+data+' '+con+' ';
			}
			else  mainval = old_val.trim()+' '+con+' '+fields+' '+' '+operator+' '+data;
		}
		$('input[name=conditions]').val(mainval);
		$('select[name=c_fields]').val('');
		$('select[name=c_operator]').val('');
		$('input[name=c_data]').val('');
	}
}

function mainDataCondition(con){
	var old_val = $('input[name=qual_field]').val();
	var fields = $('select[name=fields]').val();
	var operator = $('select[name=operator]:enabled').val();
	var data = $('span.data-span [name=data]:enabled').val();
	if(data === undefined){
		data = $('select[name=c_data]:enabled').val();
		if(data){
			if(!operator){
				operator = $('select[name=c_operend]:enabled').val();
				if(operator == '+'){
					operator = 'between';
					data = '{{today}} and {{'+data+'}}';
				}else if(operator == '-'){
					operator = 'between';
					data = '{{-'+data+'}} and {{today}} ';
				}else if(operator == '' && data == 'today'){
					operator = '=';
					data = '{{'+data+'}}';
				}
			}else{
				data = '{{'+data+'}}';
			} 
		} 
	}
	// console.log(data);
	// var dateType = $('input[name=dateType]:checked').val();
	// if(dateType) data =  $('select[name=c_data]').val();
	// if order_no then remove prefix of three chars ORS/ RFS

	// if like operator then add percentage
	if(operator == 'like') data = '%'+data+'%';
	data = is_date_open+data+is_date_close;

	var mainval="";
	if(	fields && operator && data){
		var regex = /((\bor$\b)|(\band$\b))(?!.*\1)/gi;
		if(con == 'add'){
			if(old_val.trim() && regex.exec(old_val.trim()) === null) mainval = old_val.trim()+' and '+fields+' '+' '+operator+' '+data;
			else mainval = old_val.trim()+' '+fields+' '+' '+operator+' '+data;
		}
		else{
			if(!old_val.trim()){
				mainval = old_val.trim()+' '+fields+' '+' '+operator+' '+data+' '+con+' ';
			}else if(old_val.trim() && regex.exec(old_val.trim()) == null) {
				mainval = old_val.trim()+' '+con+' '+' '+fields+' '+' '+operator+' '+data+' ';
			}
			else  mainval = con+' '+old_val.trim()+' '+fields+' '+' '+operator+' '+data;
		}
		$('input[name=qual_field]').val(mainval);
		$('select[name=fields]').val('');
		$('select[name=operator]').val('');
		$('input[name=data]').val('');
	}
}


function contentSearch(input){
	if(input && input.length){
		var is_search = input.attr('data-issearch');
		if(is_search){
			var content_field = input.attr('data-content-field');
			var tbl = input.attr('data-table');
			var title = input.attr('data-title');
			var search = input.val();
			if(search){
				$html ='<form id="content-form" action="?page=search&string='+search+'" method="post"><input type="hidden" name="table" value="'+tbl+'">'+
						'<input type="hidden" name="field" value="'+content_field+'"><input type="hidden" name="title" value="'+title+'"></form>';
				$('body').append($html);
				$('#content-form').submit();

			}else{
				alert('Please provide text to search');
			}
		}
	}
}

function insertSearchBuilder(){
	var name = $('#search-name').val();
	var query = $('#search-query').val();
	var url = $('#search-query').attr('data-url');
	var page = $('input[name=_tbl]').val();
	if(name.trim() && query){
		$.ajax({
			type:'post',
			url : url,
			data : {name:name,query:query,s_tbl:"search_criteria",page:page,url:url},
			success: function(res){
				if(res.success){
					$('#search-log').modal('hide');
					$('select[name=searched_query]').append('<option value="'+query+'">'+name+'</option>')
				}else{
					alert(res.error);

				}
			}
		})
	}

}

function parseURL(url) {
	var a = document.createElement('a');
	a.href = url;
	return {
		source: url,
		protocol: a.protocol.replace(':',''),
		host: a.hostname,
		port: a.port,
		query: a.search,
		params: (function(){
			var ret = {},
			seg = a.search.replace(/^\?/,'').split('&'),
			len = seg.length, i = 0, s;
			for (;i<len;i++) {
				if (!seg[i]) { continue; }
				s = seg[i].split('=');
				ret[s[0]] = s[1];
			}
			return ret;
		})(),
		file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
		hash: a.hash.replace('#',''),
		path: a.pathname.replace(/^([^\/])/,'/$1'),
		relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
		segments: a.pathname.replace(/^\//,'').split('/')
	};
}

function toggleInput(type,fieldid,cb){
	if(type == 'option' || type == 'list'){
		$.ajax({
			type:'post',
			url:'searchbarajax.php',
			data : {type:type,fieldid:fieldid},
			success: function(res){
				if(res){
					$('.data-span').html(res);
				}
			}
		});
	}else{
		var input = "<input type = 'text' name = 'data' placeholder = 'Enter Value'  value = '' autocomplete=\"off\">";
		$('.data-span').html(input);
	}
	cb('test');
}

function setDateRange(is_single){
	$("input[name=data]").val('');
	$("input[name=data]").datepicker("destroy");
	$("input[name=data]").daterangepicker({
		autoUpdateInput: false,
		singleDatePicker: is_single,
		"startDate": moment(),
		"endDate": "02/22/1001",
		"locale": {
			"separator" : " and ",
			cancelLabel: 'Clear'
		},
		ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Today': [moment(), moment()],
			'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
	}, function(start, end, label) {
		if(!is_single){
			$("input[name=data]").val(start.format('{{DD-MM-YYYY 00:00}}')+' and '+end.format('{{DD-MM-YYYY 23:59}}'));
		}else{
			$("input[name=data]").val(start.format('{{DD-MM-YYYY 00:00}}'));
		}
		}); 
		
}
