 <?php
 session_start();
 require_once('lib/DbModel.php');
 $db_model = new DbModel();
function msg_error($err_msg)
{   
    $_SESSION['error'] = $err_msg;
    header("location:".$_SERVER['HTTP_REFERER']);
}

if($_POST)  
{
    $tbl_name=$_POST['table_name'];
    if(empty($_POST['file_name']))
        msg_error("Enter File Name");

    if(isset($_POST['chk']))
    {
        if(empty($_POST['new_tab']))
        {    
            msg_error("Enter New Table Name");
        }    
        else
        {
            $crt_tbl=$_POST['new_tab'];
            if(empty($_POST['new_alias']))
                msg_error('Enter New Alias Name');
        }
    }
    else    
        $crt_tbl=$tbl_name;
// Connect to server and select database.
$sql="describe $tbl_name";
$result=mysql_query($sql);
$count=mysql_num_rows($result) or die(msg_error("Table $tbl_name not exists"));
 
     
$fields=array();

while($row=mysql_fetch_assoc($result))
{
    // echo $row['Field']." ".$row['Type'];
    $fields[$row['Field']] = $row['Type'];
}
 
$q="";
foreach($fields as $key=>$value)
{
   $q = $q.$key." ".$value.",";
}
$query="CREATE TABLE IF NOT EXISTS $crt_tbl"."(".rtrim($q,',').")".";";

$primary_query="SELECT k.COLUMN_NAME
FROM information_schema.table_constraints t
LEFT JOIN information_schema.key_column_usage k
USING(constraint_name,table_schema,table_name)
WHERE t.constraint_type='PRIMARY KEY'
    AND t.table_schema=DATABASE()
    AND t.table_name=\"$tbl_name\"";

$result=mysql_query($primary_query) or die(msg_error("primary key error"));
$row=mysql_fetch_assoc($result)  ;

$primary_field= $row['COLUMN_NAME'];


$alter_query="ALTER TABLE $crt_tbl ADD PRIMARY KEY (".$primary_field.");";
//$result=mysql_query($alter_query) or die(mysql_error());

$alter_query2="ALTER TABLE $crt_tbl MODIFY ".$primary_field." int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1; ";
//echo $alter_query2;



//config table
$table_data=array();$field_data=array();$field_table_data=array();$field_option_data=array();$config_table_data=array();
$event_table_data=array();
$tab_query="SELECT * FROM config where name = \"$tbl_name\"";
$result=mysql_query($tab_query) or die(msg_error("No data in config for  $tbl_name"));
$row=mysql_fetch_assoc($result);
$tbl_id=$row['tblid'];

$fields=array();
$field_options=array();
$options=array();

$script_query="SELECT * FROM events where name = \"$tbl_name\"";
$result=mysql_query($script_query) or die(msg_error("No data in events for $tbl_name"));
    while($row=mysql_fetch_assoc($result))
    {        
           array_push($event_table_data,$row);
    }
//field table
$field_query="SELECT * FROM field where tblid = \"$tbl_id\"";
$result=mysql_query($field_query) or die(msg_error("No data in field for $tbl_name"));
while($row=mysql_fetch_assoc($result))
{
       
    $fields[$row['name']]=$row['fieldid'];
    if($row['type']=="option")
         $options[$row['name']]=$row['fieldid'];
}
foreach($options as $key => $value) 
{
    $option_query="SELECT * FROM field_option where fieldid = \"$value\"";
    $result=mysql_query($option_query) or die(msg_error("No data in field_option for $tbl_name"));
    while($row=mysql_fetch_assoc($result))
    {
        $field_options[$key]=$row['optid'];   
    }
} 
// echo $tbl_name;
// print_r($fields);   
// print_r($options);  
// print_r($field_options);

$data=array();
$tab_query="SELECT name,alias,style FROM config where name = \"$tbl_name\"";
$result=mysql_query($tab_query) or die(msg_error("No data in config for  $tbl_name"));
$row=mysql_fetch_assoc($result);
$config_table_data['name']=$crt_tbl;
if(!empty($_POST['new_alias']))
    $config_table_data['alias']=$_POST['new_alias'];
else
    $config_table_data['alias']=$row['alias'];
$config_table_data['style']=$row['style'];    
 

$field_query="SELECT fieldid,name,alias,type,dbtype,dbindex,size,col,ord,span FROM field where tblid = \"$tbl_id\"";
$result=mysql_query($field_query) or die(msg_error("No data in field for $tbl_name"));
while($row=mysql_fetch_assoc($result))
{
    array_push($field_table_data,$row);       
}
foreach($options as $key => $value) 
{
    $option_query="SELECT fieldid,value,alias FROM field_option where fieldid = \"$value\"";
    $result=mysql_query($option_query) or die(msg_error("No data in field_option for $tbl_name"));
    while($row=mysql_fetch_assoc($result))
    {
        array_push($field_option_data,$row);   
    }
} 
$data["config"]=json_encode($config_table_data);
$data["field_table"]=json_encode($field_table_data);
$data["field_option_table"]=json_encode($field_option_data);
$data["event_table"]=json_encode($event_table_data);
$name=$_POST['file_name'].".txt";
$table_page="";
if(file_exists($tbl_name)) $table_page = htmlspecialchars(file_get_contents($tbl_name));
// echo $table_page;exit;
$content = $query."\n".$data["config"]."\n".$data["field_table"]."\n".$data["field_option_table"]."\n".$alter_query."\n".$alter_query2."\n".$data["event_table"]."\n".$table_page; 
// $content = "Hello world !!!";
// echo $content;exit;
if(!is_dir('migration')){
	mkdir('migration');
}
$time = date('YmdHis_');
$file = fopen('migration/'.$time.$name,"wb");
fwrite($file,$content);
fclose($file);
header('Content-type: text/plain');
header('Content-Disposition: attachment; filename="'.$name.'"');
readfile('migration/'.$time.$name);
exit();

}
?>